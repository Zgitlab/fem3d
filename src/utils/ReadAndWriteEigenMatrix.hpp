#pragma once
#include <Eigen/Core>
#include <iostream>
#include <fstream>  

namespace Eigen{
template<class Matrix>
void write_binary(const char* filename, const Matrix& matrix){
    std::ofstream out(filename, std::ios::out | std::ios::binary | std::ios::trunc);
    typename Matrix::Index rows=matrix.rows(), cols=matrix.cols();
    out.write((char*) (&rows), sizeof(typename Matrix::Index));
    out.write((char*) (&cols), sizeof(typename Matrix::Index));
    out.write((char*) matrix.data(), rows*cols*sizeof(typename Matrix::Scalar) );
    out.close();
}
template<class Matrix>
void read_binary(const char* filename, Matrix& matrix){
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    typename Matrix::Index rows=0, cols=0;
    in.read((char*) (&rows),sizeof(typename Matrix::Index));
    in.read((char*) (&cols),sizeof(typename Matrix::Index));
    matrix.resize(rows, cols);
    in.read( (char *) matrix.data() , rows*cols*sizeof(typename Matrix::Scalar) );
    in.close();
}
void IOtest()
{   
    /*Eigen::MatrixXd J = Eigen::MatrixXd::Random(10,5);
    Eigen::write_binary("matrix.dat",J);
    std::cout << "\n original \n" << J << std::endl;
    MatrixXd J_copy;
    Eigen::read_binary("matrix.dat",J_copy);
    std::cout << "\n copy \n" << J_copy << std::endl;
    std::cout.flush();
    */
    MatrixXd h01;
    Eigen::read_binary("h01.dat",h01);
    std::cout << "\n h01 \n" << h01<< std::endl;

}
} // Eigen::

