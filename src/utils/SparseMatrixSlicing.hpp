#pragma once
#include <Eigen/Sparse>
#include <iostream>
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;

namespace Eigen
{
    using namespace std;
    SpMat MatrixSlicing(vector<int> v_row, vector<int> v_col, SpMat M)
    {   
        vector<int> vM_col;
        
        std::vector<T> tripletList;

        auto OuterIndex =M.outerIndexPtr();

        //cout<<"Matrix Slicing 1"<<endl;
        // //cout<<"Number of Cols"<<sizeof(OuterIndex)/sizeof(*OuterIndex)
        // <<M.cols()<<endl;

        //put the outer index inside a vector and add the 
        //total inner number at the end
        //cout<<"Matrix Slicing 2"<<endl;
        for(int i=0; i<M.cols();i++)
        {
            vM_col.push_back(OuterIndex[i]);
        }
        vM_col.push_back(M.innerSize());
        //cout<<"Matrix Slicing 3"<<endl;
        int new_col_count=0;

        map<int, int> RowIndexOldToNew;
        for(int k;k<v_row.size();k++)
        {
            RowIndexOldToNew[v_row[k]]=k;
        };
        
        for(int i:v_col)
        {   
               
            auto InnerIndex=M.innerIndexPtr();
            vector<int> vM_row;
            for(int j=vM_col[i];j<vM_col[i+1];j++)
            {
                vM_row.push_back(InnerIndex[j]);
            }

            vector<int> v3;
            set_intersection(vM_row.begin(),vM_row.end(),
                             v_row.begin(),v_row.end(),
                             back_inserter(v3));



            for(int k:v3)
            {   
                tripletList.push_back(T(RowIndexOldToNew[k],new_col_count,M.coeff(i,k)));
                //cout<<new_row_count<<"  "<<new_col_count<< "  "<<M.coeff(i,k)<<endl; 
            };

            new_col_count++;
        }
        //cout<<"Matrix Slicing 4"<<endl;
        SpMat MatrixSliced(v_row.size(),v_col.size());

        //cout<<"Matrix Slicing 5"<<endl;
        MatrixSliced.setFromTriplets(tripletList.begin(),tripletList.end());
        //cout<<"Matrix Slicing 6"<<endl;
        return MatrixSliced;

    }
    SpMat MatrixSlicingToDense(vector<int> v_row, vector<int> v_col, SpMat M)
    {
        //SpMat A(v_row.size(),v_col.size());
        MatrixXd Ad= ((MatrixXd) M)(v_row,v_col);
        SpMat A=Ad.sparseView();
        return A;
    }
};