#pragma once

#include <Eigen/Dense>
#include <Eigen/Sparse>

using Scalar = double;

using Vec2 = Eigen::Matrix<Scalar, 2, 1>;
using Vec3 = Eigen::Matrix<Scalar, 3, 1>;
using Vec4 = Eigen::Matrix<Scalar, 4, 1>;
using Vec9 = Eigen::Matrix<Scalar, 9, 1>;

using VectorX = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

using Vec2i = Eigen::Matrix<int, 2, 1>;
using Vec3i = Eigen::Matrix<int, 3, 1>;
using Vec4i = Eigen::Matrix<int, 4, 1>;

using Matrix3 = Eigen::Matrix<Scalar, 3, 3>;
using Matrix4 = Eigen::Matrix<Scalar, 4, 4>;
using Matrix9 = Eigen::Matrix<Scalar, 9, 9>;
using Matrix12 = Eigen::Matrix<Scalar, 12, 12>;
using Matrix9_12 = Eigen::Matrix<Scalar, 9, 12>;

using DenseMatrixX = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
using DenseMatrixXi = Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic>;

using SparseMatrixX = Eigen::SparseMatrix<Scalar>;
using TripletS = Eigen::Triplet<Scalar>;



