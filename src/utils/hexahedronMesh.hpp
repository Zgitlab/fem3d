#include <Eigen/Sparse>
#include <mains/FEM/node_fem.hpp>
using namespace Eigen;

class HexahedronCuboidMesh
{   public:   

        int nx=1; //nodes along the axis
        int ny=1;
        int nz=1;
        int N_hex;
        int N_edges;
        int N_nodes;

        double unit_length=1;
        HexahedronCuboidMesh(int _nx,int _ny, int _nz, double _unit_length);

        void setPositions();
        void setHexahedrons();
        void setSurfaces();
        void setEdges();
        void setInnerProductOfDerivative();
        VectorXd getNodesPositions();
        MatrixXi getHexahedrons();
        MatrixXi getSurfaces();
        MatrixXd getSurfaceNormalVector();
        MatrixXi getEdges();
        MatrixXd getInnerProductOfDerivative();

        std::vector<bool> v_isFixed;
        
    private:
        VectorXd Positions;
        MatrixXi Hexahedrons;
        MatrixXi Edges;
        MatrixXi Surfaces;
        MatrixXd normalVector;
        MatrixXd InnerProductOfDerivative;
};

HexahedronCuboidMesh::HexahedronCuboidMesh(int _nx,int _ny, int _nz, double _unit_length)
{   
    nx=_nx;
    ny=_ny;
    nz=_nz;
    unit_length=_unit_length;
    N_nodes=nx*ny*nz;
    N_hex= (nx-1)*(ny-1)*(nz-1);
    N_edges= 12*N_hex;

    

    setPositions();    
    setHexahedrons();   
    setSurfaces();
    setEdges();
};

void HexahedronCuboidMesh::setPositions()
{
    Vector3d shift;
    shift<< (nx-1)*unit_length/2.0,  (ny-1)*unit_length/2.0, (nz-1)*unit_length/2.0;
    std::cout<<"shift"<<shift<<std::endl;
    VectorXd _Positions(3*N_nodes);
    
    
     for (int i = 0; i < nx; i++)
        {
            for (int j = 0; j < ny; j++) 
                {
                    for (int k = 0; k < nz; k++) 
                        {   
                            Vector3d a;
                            a<<i*unit_length, j*unit_length, k*unit_length;
                            _Positions.segment(3*(i*ny*nz+j*nz+k),3)= a-shift;
                            

                        };

                };
        }; 

    Positions= _Positions;     

};

void HexahedronCuboidMesh::setHexahedrons()
{
    MatrixXi _H(N_hex, 8);
    int iHex=0;
    for (int i = 0; i < nx-1; i++)
        {
            for (int j = 0; j < ny-1; j++) 
                {
                    for (int k = 0; k < nz-1; k++) 
                        { 
                            _H.row(iHex)<<  i    * ny*nz+j    *nz+k,
                                            (i+1)* ny*nz+j    *nz+k,
                                            (i+1)* ny*nz+(j+1)*nz+k,
                                            i    * ny*nz+(j+1)*nz+k,
                                            i    * ny*nz+j    *nz+k+1,
                                            (i+1)* ny*nz+j    *nz+k+1,
                                            (i+1)* ny*nz+(j+1)*nz+k+1,
                                            i    * ny*nz+(j+1)*nz+k+1;
                            iHex++;
                        };

                };
        }; 

    Hexahedrons=_H;
};

void HexahedronCuboidMesh::setSurfaces()
{   int n_s= 2*((nx-1)*(ny-1)+(ny-1)*(nz-1)+(nx-1)*(nz-1));
    MatrixXi _S(n_s,4);
    MatrixXd _n(n_s,3);
    int iSf=0;
    for (int i = 0; i < nx-1; i++)
        {
            for (int j = 0; j < ny-1; j++) 
                {   
                    Vector4i sf;
                    Vector3d _normal;
                    sf<<i*ny*nz+j*nz,
                        (i+1)*ny*nz+j*nz,
                        (i+1)*ny*nz+(j+1)*nz,
                         i*ny*nz+(j+1)*nz;

                    _normal<<0,0,-1;
                    _S.row(iSf)=sf;
                    _n.row(iSf)=_normal;
                    iSf++;
                    sf<< i*ny*nz+j*nz+nz-1,
                        (i+1)*ny*nz+j*nz+nz-1,
                        (i+1)*ny*nz+(j+1)*nz+nz-1,
                         i*ny*nz+(j+1)*nz+nz-1;
                    _normal<<0,0,1;
                    _S.row(iSf)=sf;
                     _n.row(iSf)=_normal;
                    iSf++;
                };
        };
    for (int j = 0; j < ny-1; j++) 
        {
            for (int k = 0; k < nz-1; k++) 
                { 
                     Vector4i sf;
                     Vector3d _normal;

                    sf<<j*nz+k,
                        (j+1)*nz+k,
                        (j+1)*nz+k+1,
                        j*nz+k+1;

                    _normal<<-1,0,0;
                    _S.row(iSf)=sf;
                    _n.row(iSf)=_normal;
                    iSf++;
                    sf<<j*nz+k+(nx-1)*ny*nz,
                        (j+1)*nz+k+(nx-1)*ny*nz,
                        (j+1)*nz+k+1+(nx-1)*ny*nz,
                        j*nz+k+1+(nx-1)*ny*nz;
                    _normal<< 1,0,0;
                    _S.row(iSf)=sf;
                    _n.row(iSf)=_normal;
                    iSf++;
                };
        };
        for (int i = 0; i < nx-1; i++) 
        {
            for (int k = 0; k < nz-1; k++) 
                { 
                     Vector4i sf;
                     Vector3d _normal;

                    sf<<i*ny*nz+k,
                        (i+1)*ny*nz+k,
                        (i+1)*ny*nz+k+1,
                        i*ny*nz+k+1;

                    _normal<<0,-1,0;
                    _S.row(iSf)=sf;
                    _n.row(iSf)=_normal;
                    iSf++;
                     sf<<i*ny*nz+k+(ny-1)*nz,
                        (i+1)*ny*nz+k+(ny-1)*nz,
                        (i+1)*ny*nz+k+1+(ny-1)*nz,
                        i*ny*nz+k+1+(ny-1)*nz;
                    _normal<< 0,1,0;
                    _S.row(iSf)=sf;
                    _n.row(iSf)=_normal;
                    iSf++;
                };
        };
    Surfaces=_S;
    normalVector=_n;
};
void HexahedronCuboidMesh::setEdges()
{
    MatrixXi _e(N_edges,2);
    for(int i=0;i<N_hex;i++)
    {
        MatrixXi e(12,2);
        e<< Hexahedrons(i,0),Hexahedrons(i,4),
            Hexahedrons(i,0),Hexahedrons(i,1),
            Hexahedrons(i,0),Hexahedrons(i,3),
            Hexahedrons(i,1),Hexahedrons(i,2),
            Hexahedrons(i,1),Hexahedrons(i,5),
            Hexahedrons(i,2),Hexahedrons(i,6),
            Hexahedrons(i,5),Hexahedrons(i,6),
            Hexahedrons(i,2),Hexahedrons(i,3),
            Hexahedrons(i,3),Hexahedrons(i,7),
            Hexahedrons(i,4),Hexahedrons(i,7),
            Hexahedrons(i,6),Hexahedrons(i,7),
            Hexahedrons(i,4),Hexahedrons(i,5);
        _e.block<12,2>(12*i,0)=e;
    }
    Edges=_e;
}

void HexahedronCuboidMesh::setInnerProductOfDerivative()
{   
    MatrixXd C(8,8);//connectivity matrix only for the constrained optimisation
    C<< 4,0,-1,0,0,-1,-1,-1,
        0,4,0,-1,-1,0,-1,-1,
        -1,0,4,0,-1,-1,0,-1,
        0,-1,0,4,-1,-1,-1,0,
        0,-1,-1,-1,4,0,-1,0,
        -1,0,-1,-1,0,4,0,-1,
        -1,-1,0,-1,-1,0,4,0,
        -1,-1,-1,0,0,-1,0,4;

    MatrixXd _InnerProduct(N_nodes,N_nodes);
    _InnerProduct.setZero();
    for(int i=0; i<N_hex;i++)
    {
        _InnerProduct(Hexahedrons.row(i),Hexahedrons.row(i))+=C;
    };
    InnerProductOfDerivative=_InnerProduct;
};

VectorXd HexahedronCuboidMesh::getNodesPositions()
{
    return Positions;
}
MatrixXi HexahedronCuboidMesh::getHexahedrons()
{
    return Hexahedrons;
}

MatrixXi HexahedronCuboidMesh::getSurfaces()
{
    return Surfaces;
}
MatrixXd HexahedronCuboidMesh::getSurfaceNormalVector()
{
    return normalVector;
}

MatrixXi HexahedronCuboidMesh::getEdges()
{
    return Edges;
}
MatrixXd HexahedronCuboidMesh::getInnerProductOfDerivative()
{   
    setInnerProductOfDerivative();
    return InnerProductOfDerivative;
};

