#pragma once

#include <utils/LinearAlgebra.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

namespace utils
{
    inline bool writeOBJ(const char *filePath, const DenseMatrixX &V, 
                                               const DenseMatrixXi &E, 
                                               const DenseMatrixXi &F)
    {
        std::ofstream file;
	    file.open(filePath);
	    if(file.is_open())
	    {
	    	for(unsigned i=0; i < V.rows(); ++i)
	    	{
	    		file << "v " << V(i,0)  << " " << V(i,1) << " " << V(i,2) << "\n";
	    	}

            for(unsigned i=0; i < E.rows(); ++i)
	    	{
	    		file << "l " << E(i,0) + 1 << " " << E(i,1) + 1 << "\n";
	    	}

            for(unsigned i=0; i < F.rows(); ++i)
	    	{
	    		file << "f " << F(i,0) + 1 << " " << F(i,1) + 1 << " " << F(i,2) + 1 << "\n";
	    	}

	    	file.close();
            return true;
	    }
	    else
        {
	    	std::cout << "Unable to create Obj file" << std::endl;
            return false;
        }
    }

};