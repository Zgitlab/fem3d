#pragma once

#include <utils/LinearAlgebra.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

namespace utils
{
    inline bool readOBJ(const char *filePath, DenseMatrixX &V, 
                                              DenseMatrixXi &E, 
                                              DenseMatrixXi &F)
    {
        std::ifstream file(filePath, std::ifstream::in);
        if(!file.good()) return false;
    
        std::vector<Scalar> vertexVector;
        std::vector<int> edgeIndexVector;
        std::vector<int> faceIndexVector;
        
        while(file.good())
        {
            std::string line;
            std::getline(file, line);
            std::stringstream stringstream(line);

            std::string type;
            stringstream >> type;

            if(type == "v")
            {
                Scalar x, y, z;
                stringstream >> x >> y >> z >> std::ws;

                vertexVector.push_back(x);
                vertexVector.push_back(y);
                vertexVector.push_back(z);
            }

            if(type == "l")
            {
                int id1, id2;
                stringstream >> id1 >> id2 >> std::ws;

                edgeIndexVector.push_back(id1-1);
                edgeIndexVector.push_back(id2-1);
            }

            if(type == "f")
            {
                int id1, id2, id3;
                stringstream >> id1 >> id2 >> id3 >> std::ws;

                faceIndexVector.push_back(id1-1);
                faceIndexVector.push_back(id2-1);
                faceIndexVector.push_back(id3-1);
            }
        }

        file.close();

        V.resize(vertexVector.size()/3, 3);
        for(unsigned i = 0; i < V.rows(); ++i)
        {
            V(i,0)= vertexVector[3*i];
            V(i,1)= vertexVector[3*i + 1];
            V(i,2)= vertexVector[3*i + 2];
        }
        
        E.resize(edgeIndexVector.size()/2, 2);
        for(unsigned i = 0; i < E.rows(); ++i)
        {
            E(i,0)= edgeIndexVector[2*i];
            E(i,1)= edgeIndexVector[2*i + 1];
        }

        F.resize(faceIndexVector.size()/3, 3);
        for(unsigned i = 0; i < F.rows(); ++i)
        {
            F(i,0)= faceIndexVector[3*i];
            F(i,1)= faceIndexVector[3*i + 1];
            F(i,2)= faceIndexVector[3*i + 2];
        }

        return true;
    }
};