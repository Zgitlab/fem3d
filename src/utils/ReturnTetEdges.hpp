#include <utils/LinearAlgebra.hpp>
#include <utils/RemoveRows.hpp>
#include <igl/sort.h>

DenseMatrixXi TetEdges(DenseMatrixXi Ttet)
{
	DenseMatrixXi Etet,Esort,I;
	Etet.resize(6*Ttet.rows(), 2);

	for (int i=0;i<Ttet.rows();i++)
	{
		Etet.row(6*i)   = Vec2i(Ttet(i,0), Ttet(i,1));
		Etet.row(6*i+1) = Vec2i(Ttet(i,0), Ttet(i,2));
		Etet.row(6*i+2) = Vec2i(Ttet(i,0), Ttet(i,3));
		Etet.row(6*i+3) = Vec2i(Ttet(i,1), Ttet(i,2));
		Etet.row(6*i+4) = Vec2i(Ttet(i,1), Ttet(i,3));
		Etet.row(6*i+5) = Vec2i(Ttet(i,2), Ttet(i,3));
	}


	igl::sort(Etet,1,true,Esort,I);

	int ind1=0,ind2=0;
	for (int i=Esort.rows(); i>=0;i--)
	{
		if ((ind1==Esort(i,0))&&(ind2==Esort(i,1)))
			removeRow(Esort,i+1);
		else{ind1=Esort(i,0);
			 ind2=Esort(i,1);};

	};
	removeRow(Esort,0);
	return Esort;
}

DenseMatrixXi TetEdgesFEM(DenseMatrixXi Ttet)
{
	DenseMatrixXi Etet,Esort,I;
	Etet.resize(6*Ttet.rows(), 2);

	for (int i=0;i<Ttet.rows();i++)
	{
		Etet.row(6*i)   = Vec2i(Ttet(i,0), Ttet(i,1));
		Etet.row(6*i+1) = Vec2i(Ttet(i,0), Ttet(i,2));
		Etet.row(6*i+2) = Vec2i(Ttet(i,0), Ttet(i,3));
		Etet.row(6*i+3) = Vec2i(Ttet(i,1), Ttet(i,2));
		Etet.row(6*i+4) = Vec2i(Ttet(i,1), Ttet(i,3));
		Etet.row(6*i+5) = Vec2i(Ttet(i,2), Ttet(i,3));
	}


	return Etet;
}
