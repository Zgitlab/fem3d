#pragma once
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <iostream>
#include <utils/ReadAndWriteEigenMatrix.hpp>
#include <utils/hexahedronMesh.hpp>
#include <fstream>
#include <chrono> 
using namespace std::chrono; 

using namespace std;
using namespace Eigen;
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;



void constrains_setup()
{   
    

    int Nc=8;//n_coarse_shape function
    int Nf=27;//n_fine_shape function
    int d=3;//space dimension;

    int N_vector=Nc*Nf*d*d;//vector dimension
    int N_constraints= 9*Nc*Nc;//+ 9*(Nf-Nc);//+ 9*(Nf-Nc);//+ 3*(Nf-Nc)*6;// number of constrains 

    int IndC[Nc]= {0,2,6,8,18,20,24,26}; //3*3*3

    vector<int> n1;//the effective coeficient indices
    vector<int> n2;//the redundant coeficient indices

    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {   
            for(int k=0; k<d*d;k++)
            {   
                bool b= false;
                for(int l=0;l<Nc; l++)
                {
                    b+=(j==IndC[l]);

                };
                if (b) {n2.push_back(d*d*Nf*i+d*d*j+k);}
                else {n1.push_back(d*d*Nf*i+d*d*j+k);};                                                                                          
            };
            
             
        };
    };


    //IndC<<0,1,2,3,4,5,6,7;//2*2*2
    /* loop template 
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {
            for(int k=0; k<d*d;k++)
            {

            };

        };
    };
    */

    MatrixXd vlavlavla;

    Eigen::read_binary("InnerProduct.dat",vlavlavla);
    
   

    int Hs=9*Nc*Nf;
    Eigen::read_binary("InnerProduct.dat",vlavlavla);
    SpMat H(9*Nc*Nf,9*Nc*Nf);
    std::vector<T> tripletListLa;
    std::vector<T> tripletListH;

    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {
            for(int k=0; k<d*d;k++)
            {
                for(int l=0; l<Nf;l++)
                {   
                    int r=9*Nf*i+9*j+k;
                    int c=9*Nf*i+9*l+k;
                    tripletListLa.push_back(T(r,c,vlavlavla(l,j)));
                    tripletListH.push_back(T(r,c,vlavlavla(l,j)));
                };
            };
        };
    };




    VectorXd RHS(Hs+N_constraints);
    RHS.setZero();

	Matrix3d I;
	Matrix3d Om;
	I.setIdentity();
	Om.setZero();

	VectorXd vecI(Map<VectorXd>(I.data(), 9));
	VectorXd vecO(Map<VectorXd>(Om.data(), 9));

     
    
	int TotalEquation= 0; // total equations accumulated
	/*
    //first equation
    for(int k=0; k<d*d;k++)
    {
        for(int j=0; j<(Nf-Nc); j++)
        {
            for(int i=0;i<Nc; i++)
            {  
                tripletListLa.push_back(T(Hs+9*j+k,n1[i*(Nf-Nc)*9+9*j+k],1));

                tripletListLa.push_back(T(n1[i*(Nf-Nc)*9+9*j+k],Hs+9*j+k,1));
            };
        };
    };

    //RHS 
    
    //first equation 
    for(int j=0; j<(Nf-Nc);j++)
    {   
        RHS.segment(Hs+9*j,9)=vecI;
    };

    TotalEquation+= 9*(Nf-Nc);
    
    
    //LHS
	/*
    
    //second equation
	HexahedronCuboidMesh Mesh(3, 3, 3, 1);
	VectorXd  X;
	X=Mesh.getNodesPositions();
    cout<<X<<endl;
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<(Nf-Nc);j++)
        {   
            int s1= Hs+ TotalEquation +j*9;//shift_row
            int s2=i*(Nf-Nc)*9+j*9;//shift_col
            Vector3d a=X.segment(3*IndC[i],3);
            tripletListLa.push_back(T(  s1,n1[s2+1],-a(2)));
			tripletListLa.push_back(T(  s1,n1[s2+2], a(0)));
			tripletListLa.push_back(T(s1+1,n1[s2+4],-a(2)));
			tripletListLa.push_back(T(s1+1,n1[s2+5], a(0)));
            tripletListLa.push_back(T(s1+2,n1[s2+7],-a(2)));
            tripletListLa.push_back(T(s1+2,n1[s2+8], a(0)));
            tripletListLa.push_back(T(s1+3,n1[s2+0], a(2)));
            tripletListLa.push_back(T(s1+3,n1[s2+2],-a(1)));
            tripletListLa.push_back(T(s1+4,n1[s2+3], a(2)));
            tripletListLa.push_back(T(s1+4,n1[s2+5],-a(1)));
            tripletListLa.push_back(T(s1+5,n1[s2+6], a(2)));
            tripletListLa.push_back(T(s1+5,n1[s2+8],-a(1)));
            tripletListLa.push_back(T(s1+6,n1[s2+0],-a(0)));
            tripletListLa.push_back(T(s1+6,n1[s2+1], a(1)));
            tripletListLa.push_back(T(s1+7,n1[s2+3],-a(0)));
            tripletListLa.push_back(T(s1+7,n1[s2+4], a(1)));
            tripletListLa.push_back(T(s1+8,n1[s2+6],-a(0)));
            tripletListLa.push_back(T(s1+8,n1[s2+7], a(1)));

            tripletListLa.push_back(T(n1[s2+1],  s1,-a(2)));
			tripletListLa.push_back(T(n1[s2+2],  s1, a(0)));
			tripletListLa.push_back(T(n1[s2+4],s1+1,-a(2)));
			tripletListLa.push_back(T(n1[s2+5],s1+1, a(0)));
            tripletListLa.push_back(T(n1[s2+7],s1+2,-a(2)));
            tripletListLa.push_back(T(n1[s2+8],s1+2, a(0)));
            tripletListLa.push_back(T(n1[s2+0],s1+3, a(2)));
            tripletListLa.push_back(T(n1[s2+2],s1+3,-a(1)));
            tripletListLa.push_back(T(n1[s2+3],s1+4, a(2)));
            tripletListLa.push_back(T(n1[s2+5],s1+4,-a(1)));
            tripletListLa.push_back(T(n1[s2+6],s1+5, a(2)));
            tripletListLa.push_back(T(n1[s2+8],s1+5,-a(1)));
            tripletListLa.push_back(T(n1[s2+0],s1+6,-a(0)));
            tripletListLa.push_back(T(n1[s2+1],s1+6, a(1)));
            tripletListLa.push_back(T(n1[s2+3],s1+7,-a(0)));
            tripletListLa.push_back(T(n1[s2+4],s1+7, a(1)));
            tripletListLa.push_back(T(n1[s2+6],s1+8,-a(0)));
            tripletListLa.push_back(T(n1[s2+7],s1+8, a(1)));
        };
    };
    //RHS
    //second equation

    for (int j=0; j<(Nf-Nc);j++)
    {   Matrix3d X_cross;
        VectorXd vecX(9);
        vecX<<0,-X(3*j+2),X(3*j+1),
              X(3*j+2),0,-X(3*j),
              -X(3*j+1),X(3*j),0;

        RHS.segment(Hs + TotalEquation +9*j,9)=vecX;
    };
	TotalEquation += 9 * (Nf-Nc);
	
     cout<<"here3"<<endl;

    /*
    //LHS
    
    //Third equation
    
    int Nab=6;
    MatrixXd Hab[Nab];//displacement function, dimension Nf,3
    char FileName[6][10]={"h00.dat","h01.dat","h02.dat","h11.dat","h12.dat","h22.dat"};
    for(int ab=0; ab<6 ;ab++)
    {
         Eigen::read_binary(FileName[ab],Hab[ab]);  
    };

    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<(Nf-Nc);j++)
        {
            for(int ab=0; ab<6 ;ab++)
            {
                int s1= Hs + TotalEquation +3*j*6+3*ab;
                int s2=9*(Nf-Nc)*i+9*j;

                tripletListLa.push_back(T(  s1,  s2,Hab[ab](IndC(i),0)));
                tripletListLa.push_back(T(  s1,s2+1,Hab[ab](IndC(i),1)));
                tripletListLa.push_back(T(  s1,s2+2,Hab[ab](IndC(i),2)));
                tripletListLa.push_back(T(s1+1,s2+3,Hab[ab](IndC(i),0)));
                tripletListLa.push_back(T(s1+1,s2+4,Hab[ab](IndC(i),1)));
                tripletListLa.push_back(T(s1+1,s2+5,Hab[ab](IndC(i),2)));
                tripletListLa.push_back(T(s1+2,s2+6,Hab[ab](IndC(i),0)));
                tripletListLa.push_back(T(s1+2,s2+7,Hab[ab](IndC(i),1)));
                tripletListLa.push_back(T(s1+2,s2+8,Hab[ab](IndC(i),2)));


                tripletListLa.push_back(T(  s2,  s1,Hab[ab](IndC(i),0)));
                tripletListLa.push_back(T(s2+1,  s1,Hab[ab](IndC(i),1)));
                tripletListLa.push_back(T(s2+2,  s1,Hab[ab](IndC(i),2)));
                tripletListLa.push_back(T(s2+3,s1+1,Hab[ab](IndC(i),0)));
                tripletListLa.push_back(T(s2+4,s1+1,Hab[ab](IndC(i),1)));
                tripletListLa.push_back(T(s2+5,s1+1,Hab[ab](IndC(i),2)));
                tripletListLa.push_back(T(s2+6,s1+2,Hab[ab](IndC(i),0)));
                tripletListLa.push_back(T(s2+7,s1+2,Hab[ab](IndC(i),1)));
                tripletListLa.push_back(T(s2+8,s1+2,Hab[ab](IndC(i),2)));
            };

        };
    };

    //RHS
	//Third equation
    for (int j=0; j<Nf;j++)
    {
        for (int ab=0; ab<6; ab++)
        {   Vector3d seg; 
            seg=Hab[ab].row(j).transpose();
            RHS.segment(Hs + TotalEquation +3*j*6+3*ab,3)=seg;
        };
    };
	
   
	TotalEquation += 3 * (Nf-Nc) * 6;
	*/
    
    //LHS
    //Forth equation
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nc;j++)
        {
            for(int k=0; k<d*d;k++)
            {
                 int s1= Hs+TotalEquation +(9*Nc*i+9*j);//shift_row
                 int s2=9*Nf*i+9*IndC[j];//shift_col
                 tripletListLa.push_back(T(s1+k,s2+k,1));
                 tripletListLa.push_back(T(s2+k,s1+k,1));
            };
        };
    }; 

    // RHS
    ///Forth equation
    for(int i=0;i<Nc; i++) 
        {
            for(int j=0;j<Nc; j++)
            {
                if(i==j)
                    {
                         RHS.segment(Hs+TotalEquation +9*(i*Nc+j),9)=vecI;
                    }
                else 
                    {
                         RHS.segment(Hs+TotalEquation +9*(i*Nc+j),9)=vecO;
                    };
            };
        };
    

    TotalEquation+= d*d*Nc*Nc;
    cout<<"here4"<<endl;
    auto start = high_resolution_clock::now(); 
    SpMat Lagrangian(Hs+N_constraints,Hs+N_constraints);

    Lagrangian.setFromTriplets(tripletListLa.begin(), tripletListLa.end());
    H.setFromTriplets(tripletListH.begin(), tripletListH.end());
    cout<<"here5"<<endl;

    //Eigen::SimplicialCholesky<SpMat> chol(Lagrangian);
    SparseLU<SparseMatrix<double>, COLAMDOrdering<int> >   solver(Lagrangian);
	Eigen::VectorXd x = solver.solve(RHS);
    auto stop = high_resolution_clock::now(); 
    //
    cout <<"hola"<<endl;
    VectorXd ne=x.segment(0,Hs);
    double c= ne.transpose()*H*ne;
    cout<<c/6<<endl;
    MatrixXd Lm;
    Lm=MatrixXd(Lagrangian);
    cout<<ne<<endl;
    std::ofstream file4("b.txt");
    if (file4.is_open())
    {
    file4 << "Here is the vector b:\n" << RHS.segment(Hs,N_constraints) << '\n';
    }
    std::ofstream file1("ne.txt");
    if (file1.is_open())
    {
    file1 << "Here is the vector ne:\n" << ne << '\n';
    }

    std::ofstream file2("H.txt");
    if (file2.is_open())
    {
    file2 << "Here is the matrix H:\n" << Lm.block(0,0,Hs,Hs) << '\n';
    }

    std::ofstream file3("A.txt");
    if (file3.is_open())
    {
    file3 << "Here is the matrix A:\n" << Lm.block(Hs,0,N_constraints,Hs) << '\n';
    }
    Eigen::write_binary("ne.dat",ne); 
    Eigen::write_binary("H.dat",Lm.block(0,0,Hs,Hs)); 
    Eigen::write_binary("A.dat",Lm.block(Hs,0,N_constraints,Hs));
    //auto duration = duration_cast<microseconds>(stop - start); 
    //cout << duration.count() <<"micro seconds"<< endl; 
}
