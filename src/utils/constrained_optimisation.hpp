#pragma once
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <iostream>
#include <utils/ReadAndWriteEigenMatrix.hpp>
#include <utils/hexahedronMesh.hpp>

using namespace std;
using namespace Eigen;
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;


void constrains_setup()
{

    int Nc=8;//n_coarse_shape function
    int Nf=27;//n_fine_shape function
    int d=3;//space dimension;

    int N_vector=Nc*Nf*d*d;//vector dimension
    int N_constraints=     9*Nf+ 9*Nf+ 3*Nf*6 +  9* Nc*Nc;// number of constrains 
    VectorXi IndC(Nc);//Coarse Nodes Index
    IndC<<0,2,6,8,18,20,24,26; //3*3*3

    //IndC<<0,1,2,3,4,5,6,7;//2*2*2

    VectorXd ne0(N_vector);
    /* loop template 
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {
            for(int k=0; k<d*d;k++)
            {

            };

        };
    };
    */


    
    cout<<"here1"<<endl;

   
    SpMat LHS(N_constraints,N_vector);
    VectorXd RHS(N_constraints);

	Matrix3d I;
	Matrix3d Om;
	I.setIdentity();
	Om.setZero();

	VectorXd vecI(Map<VectorXd>(I.data(), 9));
	VectorXd vecO(Map<VectorXd>(Om.data(), 9));

     //LHS
    std::vector<T> tripletList;
    
	int TotalEquation = 0;
	
    //first equation
    for(int k=0; k<d*d;k++)
    {
        for(int j=0; j<Nf;j++)
        {
            for(int i=0;i<Nc; i++)
            {  
                tripletList.push_back(T(9*j+k,i*Nf*9+9*j+k,1));
            };
        };
    };

    //RHS 
    
    //first equation
    
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {   
        RHS.segment(9*j,9)=vecI;
        };
    };
    TotalEquation += 9 * Nf;

    cout<<"here2"<<endl;
	//
    
    //LHS
	
    //second equation
	HexahedronCuboidMesh Mesh(3, 3, 3, 1);
	VectorXd  X;
	X=Mesh.getNodesPositions();
    cout<<X<<endl;
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {   
            int s1= TotalEquation +j*9;//shift_row
            int s2=i*Nf*9+j*9;//shift_col
            Vector3d a=X.segment(3*IndC(i),3);
            tripletList.push_back(T(  s1,s2+1,-a(2)));
			tripletList.push_back(T(  s1,s2+2, a(0)));
			tripletList.push_back(T(s1+1,s2+4,-a(2)));
			tripletList.push_back(T(s1+1,s2+5, a(0)));
            tripletList.push_back(T(s1+2,s2+7,-a(2)));
            tripletList.push_back(T(s1+2,s2+8, a(0)));
            tripletList.push_back(T(s1+3,s2+0, a(2)));
            tripletList.push_back(T(s1+3,s2+2,-a(1)));
            tripletList.push_back(T(s1+4,s2+3, a(2)));
            tripletList.push_back(T(s1+4,s2+5,-a(1)));
            tripletList.push_back(T(s1+5,s2+6, a(2)));
            tripletList.push_back(T(s1+5,s2+8,-a(1)));
            tripletList.push_back(T(s1+6,s2+0,-a(0)));
            tripletList.push_back(T(s1+6,s2+1, a(1)));
            tripletList.push_back(T(s1+7,s2+3,-a(0)));
            tripletList.push_back(T(s1+7,s2+4, a(1)));
            tripletList.push_back(T(s1+8,s2+6,-a(0)));
            tripletList.push_back(T(s1+8,s2+7, a(1)));
        };
    };
    //RHS
    //second equation

    for (int j=0; j<Nf;j++)
    {   Matrix3d X_cross;
        VectorXd vecX(9);
        vecX<<0,-X(3*j+2),X(3*j+1),
              X(3*j+2),0,-X(3*j),
              -X(3*j+1),X(3*j),0;

        RHS.segment(TotalEquation +9*j,9)=vecX;
    };
	TotalEquation += 9 * Nf;
	
     cout<<"here3"<<endl;


    //LHS
    
    //Third equation
    
    int Nab=6;
    MatrixXd Hab[Nab];//displacement function, dimension Nf,3
    char FileName[6][10]={"h00.dat","h01.dat","h02.dat","h11.dat","h12.dat","h22.dat"};
    for(int ab=0; ab<6 ;ab++)
    {
         Eigen::read_binary(FileName[ab],Hab[ab]);  
    };

    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {
            for(int ab=0; ab<6 ;ab++)
            {
                int s1= TotalEquation +3*j*6+3*ab;
                int s2=9*Nf*i+9*j;
                tripletList.push_back(T(s1,s2,Hab[ab](IndC(i),0)));
                tripletList.push_back(T(s1,s2+1,Hab[ab](IndC(i),1)));
                tripletList.push_back(T(s1,s2+2,Hab[ab](IndC(i),2)));
                tripletList.push_back(T(s1+1,s2+3,Hab[ab](IndC(i),0)));
                tripletList.push_back(T(s1+1,s2+4,Hab[ab](IndC(i),1)));
                tripletList.push_back(T(s1+1,s2+5,Hab[ab](IndC(i),2)));
                tripletList.push_back(T(s1+2,s2+6,Hab[ab](IndC(i),0)));
                tripletList.push_back(T(s1+2,s2+7,Hab[ab](IndC(i),1)));
                tripletList.push_back(T(s1+2,s2+8,Hab[ab](IndC(i),2)));
            };

        };
    };

    //RHS
	//Third equation
    for (int j=0; j<Nf;j++)
    {
        for (int ab=0; ab<6; ab++)
        {   Vector3d seg; 
            seg=Hab[ab].row(j).transpose();
            RHS.segment(TotalEquation +3*j*6+3*ab,3)=seg;
        };


    }
	
   
	TotalEquation += 3 * Nf * 6;
	
  
    cout<<"here4"<<endl;
    //LHS
    //Forth equation
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nc;j++)
        {
            for(int k=0; k<d*d;k++)
            {
                 int s1= TotalEquation +(9*Nc*i+9*j);//shift_row
                 int s2=9*Nf*i+9*IndC(j);//shift_col
                 tripletList.push_back(T(s1+k,s2+k,1));

            };

        };
    }; 
    
    
    cout<<"here4.1"<<endl;
    
    
    //RHS
    //Forth equation
    for(int i=0;i<Nc; i++) 
        {
            for(int j=0;j<Nc; j++)
            {
                if(i==j)
                    {
                         RHS.segment(TotalEquation +9*(i*Nc+j),9)=vecI;
                    }
                else 
                    {
                         RHS.segment(TotalEquation +9*(i*Nc+j),9)=vecO;
                    };
            };
        };

    cout<<"here4.2"<<endl;
     
    /*
   LHS.setFromTriplets(tripletList.begin(), tripletList.end());
    
    cout<<"here5"<<endl;
    //solver testify
    SparseQR<SpMat,COLAMDOrdering<int>> QR;
    SpMat LHST;
    LHST=LHS.transpose();
    LHST.makeCompressed();
    cout<<"here6"<<endl;    
    cout<<LHS.rows()<<"  "<<LHS.cols()<<endl;
    cout<<LHST.rows()<<"  "<<LHST.cols()<<endl;
    QR.compute(LHST);
	cout << "Kernel size" << LHS.cols() - QR.rank() << endl;
    SpMat Q;
    Q=QR.matrixQ();
    SpMat Kernel,Q1;
    cout<<"here7"<<endl;
    Kernel=Q.rightCols(LHS.cols()-QR.rank());
    Q1=Q.leftCols(QR.rank());
    cout<<"Kernel"<<Kernel.rows()<<Kernel.cols()<<endl;

    
    QR.compute(LHS);
    ne0=QR.solve(RHS);

    //Eigen::write_binary("Kernel.dat",Kernel); 
    Eigen::write_binary("notimportant2.dat",ne0); 
    */
    MatrixXd vlavlavla;

    Eigen::read_binary("InnerProduct.dat",vlavlavla);
    SpMat A(9*Nc*Nf,9*Nc*Nf);
    std::vector<T> tripletList3;
    
    double sum1=0;

    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {
            for(int k=0; k<d*d;k++)
            {
                for(int l=0; l<Nf;l++)
                {   
                    int r=9*Nf*i+9*j+k;
                    int c=9*Nf*i+9*l+k;
                    tripletList3.push_back(T(r,c,vlavlavla(l,j)));
                    sum1+=abs(vlavlavla(l,j)/6.0);
                    
                };
            };
        };
    };

    cout<<sum1<<"sum"<<endl;
    /*
    A.setFromTriplets(tripletList3.begin(), tripletList3.end());
    VectorXd minusane0;
    minusane0=-Kernel.transpose()*A*ne0;
    QR.compute(Kernel.transpose()*A*Kernel);
    VectorXd q,ne;
    q=QR.solve(minusane0);
    ne=Kernel*q+ne0;
	for (int i=0; i < ne.size(); i++)
	{
		if (abs(ne(i)) < 1e-12)
			ne(i) = 0;

	}
    Eigen::write_binary("notimportant.dat",ne); 
	cout <<ne<< endl;
    */
}
