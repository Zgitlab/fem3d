
#include <rendering/renderables/Renderable3D.hpp>

Renderable3D::Renderable3D()
: m_position(glm::vec3(0.0, 0.0, 0.0)),
  m_posDelta(glm::vec3(0.0, 0.0, 0.0)),
  m_eulerAngles(glm::vec3(0.0, 0.0, 0.0)),
  m_scale(glm::vec3(1.0, 1.0, 1.0))
{

}

Renderable3D::~Renderable3D()
{

}

void Renderable3D::draw(Camera *camera)
{

}

void Renderable3D::drawGui()
{
    /*
    ImGui::PushID(this);

    const std::string name = getName() + " (Renderable3D)";
    if(ImGui::CollapsingHeader(name.data()))
    {
        ImGui::Indent(16.0f);

        ImGui::Text("position = %f, %f, %f", m_position.x, m_position.y, m_position.z);
        ImGui::Text("pos Delta = %f, %f, %f", m_posDelta.x, m_posDelta.y, m_posDelta.z);
        ImGui::Text("euler Angles = %f, %f, %f", m_eulerAngles.x, m_eulerAngles.y, m_eulerAngles.z);
        ImGui::Text("scale = %f, %f, %f", m_scale.x, m_scale.y, m_scale.z);
        
        ImGui::Unindent(16.0f);
    }

    ImGui::PopID();
    */
}

void Renderable3D::getModelMatrix(glm::mat4 &m) const
{   
     m = glm::translate(m_position + m_posDelta) * 
         glm::eulerAngleYXZ(m_eulerAngles.y, m_eulerAngles.x, m_eulerAngles.z) * 
         glm::scale(m_scale);
}
