#pragma once

#include <rendering/baseClasses/INameable.hpp>
#include <rendering/baseClasses/IRenderable.hpp>
#include <rendering/baseClasses/IGui.hpp>

class Renderable3D: public INameable, public IRenderable, public IGui
{
public:

    Renderable3D();
    virtual ~Renderable3D();

    //
    virtual void draw(Camera *camera) override;
    virtual void drawGui() override;

    //
    void getModelMatrix(glm::mat4 &m) const;
    
public:

    glm::vec3 m_position, m_posDelta;
    glm::vec3 m_eulerAngles;
    glm::vec3 m_scale;

};
