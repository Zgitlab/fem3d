#pragma once

#include <rendering/baseClasses/Camera.hpp>

class IRenderable
{
public:
    
    virtual void initRenderable() {};

    //
    virtual void draw(Camera *camera) = 0;
};