
#include <rendering/baseClasses/Window.hpp>
#include <iostream>

Camera Window::m_camera;

struct KeyboardState
{
    bool shiftPressed = false;

} Window::m_keyboardState;

struct MouseState
{
    bool middlePressed = false;
    double xRefPos, yRefPos;

} Window::m_mouseState;

void Window::windowSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    m_camera.updateProj(60.0f, (float)width/(float)height);
}

void Window::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS)
        m_keyboardState.shiftPressed = true;
    if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_RELEASE)
        m_keyboardState.shiftPressed = false;
}

void Window::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
    {
        m_mouseState.middlePressed = true;

        glfwGetCursorPos(window, &m_mouseState.xRefPos,
                                 &m_mouseState.yRefPos); 
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE)
    {
        m_mouseState.middlePressed = false;
        m_camera.saveStateAsRef();
    }
}

void Window::scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    m_camera.orbit(glm::vec3(0.0, 0.0, -0.09*m_camera.m_spherical[2]*yoffset));
    m_camera.saveStateAsRef();
}

void GLAPIENTRY messageCallback( GLenum source,
                                 GLenum type,
                                 GLuint id,
                                 GLenum severity,
                                 GLsizei length,
                                 const GLchar* message,
                                 const void* userParam )
{
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
}

/////////////////////////////////////////////////////////

Window::Window(const char *title)
{
    glfwInit();

    glfwWindowHint(GLFW_SAMPLES, 8);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    m_window = glfwCreateWindow(500, 500, title, nullptr, nullptr);

    glfwMakeContextCurrent(m_window);

    //glfwSwapInterval(0);

    glfwSetWindowSizeCallback(m_window, windowSizeCallback);
    glfwSetKeyCallback(m_window, keyCallback);
    glfwSetMouseButtonCallback(m_window, mouseButtonCallback);
    glfwSetScrollCallback(m_window, scrollCallback);

    glewExperimental = GL_TRUE;
    glewInit();
    
    std::cout << "OpenGL: " << glGetString(GL_VERSION) << std::endl;

    //glEnable(GL_DEBUG_OUTPUT);
    //glDebugMessageCallback(messageCallback, 0);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    
    //ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    //Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(m_window, true);
    ImGui_ImplOpenGL3_Init("#version 330");

    m_guiFrameInProgress = false;

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.3, 0.3, 0.3, 0.0);
    //glClearColor(1.0, 1.0, 1.0, 0.0);

    glGenVertexArrays(1, &m_vaoId);
    m_gridProgram.load("src/rendering/shaders/gridShader.vert", "src/rendering/shaders/colorShader.frag");
}

Window::~Window()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(m_window);
    glfwTerminate();
}

void Window::manageEvents()
{
    if(m_mouseState.middlePressed)
    {
        double xpos, ypos;
        glfwGetCursorPos(m_window, &xpos, &ypos);
        
        double dx = xpos - m_mouseState.xRefPos;
        double dy = ypos - m_mouseState.yRefPos;

        if(m_keyboardState.shiftPressed)
        {
            float fac = 0.0025*m_camera.m_spherical[2];

            m_camera.translate(glm::vec2(-fac*dx, fac*dy));
        }
        else
        {
            m_camera.orbit(glm::vec3(0.01*dy, 0.01*dx, 0.0));
        }
    }
}

void Window::draw(IRenderable *renderable)
{
    renderable->draw(&m_camera);
}

void Window::drawGrid()
{
    if(m_renderGrid)
    {
        const glm::mat4 modelX = glm::mat4(1.0);
        const glm::mat4 modelY = glm::rotate(glm::radians(90.0f), glm::vec3(0.0, 1.0, 0.0));
    
        glBindVertexArray(m_vaoId);
        m_gridProgram.bind();
        m_gridProgram.setUniform("color", glm::vec3(0.22,0.22,0.22));
        m_gridProgram.setUniform("projView", m_camera.getProjView());
    
        m_gridProgram.setUniform("model", modelX);
        glDrawArrays(GL_LINES, 0, 30);
    
        m_gridProgram.setUniform("model", modelY);
        glDrawArrays(GL_LINES, 0, 30);
    }
}

void Window::drawGui(IGui *gui)
{
    if(m_guiFrameInProgress == false)
    {
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        m_guiFrameInProgress = true;

        ImGui::Begin("");

        if(ImGui::CollapsingHeader("Camera"))
        {   
            ImGui::Indent(16.0f);
            ImGui::DragScalar("target x", ImGuiDataType_Float, &(m_camera.m_target.x), 0.01);
            ImGui::DragScalar("target y", ImGuiDataType_Float, &(m_camera.m_target.y), 0.01);
            ImGui::DragScalar("target z", ImGuiDataType_Float, &(m_camera.m_target.z), 0.01);

            ImGui::Separator();

            ImGui::DragScalar("angle x", ImGuiDataType_Float, &(m_camera.m_spherical.x), 0.01);
            ImGui::DragScalar("angle y", ImGuiDataType_Float, &(m_camera.m_spherical.y), 0.01);
            ImGui::DragScalar("distance", ImGuiDataType_Float, &(m_camera.m_spherical.z), 0.01);

            ImGui::Separator();

            ImGui::Checkbox("render grid", &m_renderGrid);

            ImGui::Unindent(16.0f);
        }

        m_camera.updateView();
    }

    gui->drawGui();

}

void Window::renderGui()
{   
    ImGui::End();
    
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    
    m_guiFrameInProgress = false;
}
