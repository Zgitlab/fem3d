#pragma once

#include <string>

class INameable
{ 
public:

    INameable()
    :m_name("*"){}

    //
    inline void setName(const std::string &name)
    {
        m_name = name;
    }

    inline const std::string &getName() const
    {
        return m_name;
    }

private:

    std::string m_name;

};