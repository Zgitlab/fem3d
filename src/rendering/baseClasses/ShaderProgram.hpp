#pragma once

#include <GL/glew.h>

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

class ShaderProgram
{
public:

    ShaderProgram();
    ~ShaderProgram();

    void load(const char *vertexPath, const char *fragmentPath);

    void bind();

    void setUniform(const GLchar *name, float value);
    void setUniform(const GLchar *name, glm::vec3 value);
    void setUniform(const GLchar *name, glm::mat4 value);

private:

    unsigned programId;
};