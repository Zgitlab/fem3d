
#include <rendering/baseClasses/ShaderProgram.hpp>
#include <iostream>
#include <fstream>

char *loadStringFromFile(const char *fileName, unsigned int &fileLen)
{
	std::ifstream file;
	file.open(fileName, std::ios::in);
	if (!file) return 0;

	file.seekg(0, std::ios::end);
	fileLen = unsigned(file.tellg());
	file.seekg(std::ios::beg);

	char *source = new char[fileLen + 1];

	int i = 0;
	while (file.good())
	{
		source[i] = char(file.get());
		if (!file.eof()) i++;
		else fileLen = i;
	}
	source[fileLen] = '\0';
	file.close();

	return source;
}

GLuint compileShader(const char *fileName, GLenum type)
{ 
	unsigned int fileLen;
	char *source = loadStringFromFile(fileName, fileLen);
	if(source == 0)
	{
		std::cout << "ERROR: " << fileName << " not found" << std::endl;
		exit(-1);
	}

	//////////////////////////////////////////////
	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1,
		(const GLchar **)&source, (const GLint *)&fileLen);
	glCompileShader(shader);
	delete[] source;

	//////////////////////////////////////////////
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, nullptr, logString);
		std::cout << "ERROR: " << logString << std::endl;
		delete[] logString;
		glDeleteShader(shader);
		exit(-1);
	}

	return shader;
}

/////////////////////////////////////////////////////

ShaderProgram::ShaderProgram()
{

}

ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(programId);
}

void ShaderProgram::load(const char *vertexPath, const char *fragmentPath)
{
    GLuint vshaderId = compileShader(vertexPath, GL_VERTEX_SHADER);
    GLuint fshaderId = compileShader(fragmentPath, GL_FRAGMENT_SHADER);

    programId = glCreateProgram();
    glAttachShader(programId, vshaderId);
    glAttachShader(programId, fshaderId);
    glLinkProgram(programId);

	int linked;
	glGetProgramiv(programId, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		GLint logLen;
		glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(programId, logLen, nullptr, logString);
		std::cout << "ERROR: " << logString << std::endl;
		delete[] logString;
		glDeleteProgram(programId);
		programId = 0;
		exit(-1);
	}

	glDetachShader(programId, vshaderId);
	glDetachShader(programId, fshaderId);
	
	glDeleteShader(vshaderId);
	glDeleteShader(fshaderId);
}

void ShaderProgram::bind()
{
	glUseProgram(programId);
}

void ShaderProgram::setUniform(const GLchar *name, float value)
{
	GLuint loc = glGetUniformLocation(programId, name);
    glUniform1f(loc, value);
}

void ShaderProgram::setUniform(const GLchar *name, glm::vec3 value)
{
	GLuint loc = glGetUniformLocation(programId, name);
    glUniform3fv(loc, 1, &value[0]);
}

void ShaderProgram::setUniform(const GLchar *name, glm::mat4 value)
{
	GLuint loc = glGetUniformLocation(programId, name);
    glUniformMatrix4fv(loc, 1, GL_FALSE, &value[0][0]);
}