#pragma once

#include <GL/glew.h>

class Buffer
{
public:

    Buffer()
    {

    }

    ~Buffer()
    {
        glDeleteBuffers(1, &m_id);
    }

    inline void setup(GLenum target, GLenum usage)
    {   
        m_target = target;
        m_usage = usage;
        glGenBuffers(1, &m_id);
        glBindBuffer(target, m_id);
    }

    inline void bind()
    {
        glBindBuffer(m_target, m_id);
    }

    inline void loadData(const void *data, unsigned size)
    {
        bind();
        m_size = size;
        glBufferData(m_target, size, data, m_usage);
    }

    inline unsigned size()
    {
        return m_size;
    }

private:

    unsigned m_id;
    unsigned m_size;

    GLenum m_target;
    GLenum m_usage;

};