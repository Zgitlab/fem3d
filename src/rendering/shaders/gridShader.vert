#version 330 core

uniform mat4 projView;
uniform mat4 model;

out vec3 fpos;

void main()
{
  int ix = gl_VertexID/2;
  int iz = gl_VertexID - 2*(gl_VertexID/2);

  vec3 pos = vec3(ix, 0.0, 16*iz) - vec3(7, 0.0, 8);

  gl_Position = projView * model * vec4(pos, 1.0);
}
