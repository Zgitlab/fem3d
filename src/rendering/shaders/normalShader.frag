#version 330 core

uniform vec3 color;

out vec4 FragColor;

in vec3 fpos;

void main()
{
    vec3 dFdxPos = dFdx( fpos );
	vec3 dFdyPos = dFdy( fpos );
	vec3 normal = normalize( cross(dFdxPos, dFdyPos));

    vec3 L1 = normalize(vec3(-1.0, 0.0, 1.0));
    vec3 L2 = normalize(vec3( 1.0, 1.0, 1.0));
    vec3 L3 = normalize(vec3( 2.0, -10.0, 0.0));

    //const vec3 I1 = vec3( 0.8, 0.8, 0.8);
    //const vec3 I2 = vec3( 0.5, 0.5, 0.6);
    //const vec3 I3 = vec3( 0.8, 0.85, 1.0);

    vec3 I1 = vec3(0.8);
    vec3 I2 = vec3(0.2);
    vec3 I3 = vec3(0.1);

    vec3 diffuse1 = I1 * max(0.0, dot(normal, L1));
    vec3 diffuse2 = I2 * max(0.0, dot(normal, L2));
    vec3 diffuse3 = I3 * max(0.0, dot(normal, L3));

    vec3 diffuse = diffuse1 + diffuse2 + diffuse3;
    vec3 colorCorrected = pow(color * diffuse, vec3(1.0/2.2));
    FragColor = vec4(colorCorrected, 1.0);
} 
