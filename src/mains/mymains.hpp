
#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>
#include <utils/io/readOBJ.hpp>
#include <mains/FEM/Integrator_fem.hpp>
#include <mains/FEM/Integrator_fem.hpp>
#include <mains/FEM/tetrahedron_fem.hpp>
#include <mains/FEM/force_fem.hpp>
#include <mains/FEM/force_type/NeoHookean.hpp>
#include <mains/FEM/system_fem.hpp>
#include <igl/copyleft/tetgen/tetrahedralize.h>
#include <utils/ReturnTetEdges.hpp>


using namespace std;


namespace Mains
{

VectorXd HarmonicDisplacement(MatrixXi Ftet, Matrix3d StressTensor, systemFEM sF)
{
	Node* nodes=sF.ptr_nodes;
	int n_nodes=sF.n_nodes;
	MatrixXd Jacobian= sF.getForceJacobian();
	VectorXd LoadingForce;
	LoadingForce=VectorXd::Zero(3*n_nodes);
	for (int i=0; i<Ftet.rows(); i++)
	{
		Vector3i INDFaceNodes=Ftet.row(i);

		Vector3d x0=nodes[INDFaceNodes(0)].getPosition();
		Vector3d x1=nodes[INDFaceNodes(1)].getPosition();
		Vector3d x2=nodes[INDFaceNodes(2)].getPosition();
		Vector3d A=(x1-x0).cross(x2-x0);
	  Vector3d f= StressTensor* A;

		if(f.norm()>1e-6)
		{
			LoadingForce.segment(3*INDFaceNodes(0),3)+=f/3;
			LoadingForce.segment(3*INDFaceNodes(1),3)+=f/3;
			LoadingForce.segment(3*INDFaceNodes(2),3)+=f/3;
		};		
	};

	VectorXd displacement;
	displacement= -Jacobian.completeOrthogonalDecomposition().pseudoInverse()* LoadingForce;


return displacement;
};

void HarmonicTest()
{
	Window window("Visualizer");
	window.setSize(1200, 700);
	DenseMatrixX V1,V,A; // Vertices
	DenseMatrixXi E1, F1,Esort; // Edges, Faces

	utils::readOBJ("meshes/cubefine.obj", V1, E1, F1);


	DenseMatrixX Vtet,I; // Vertices
	DenseMatrixXi Ttet, Etet, Ftet; // Edges, Faces

	int status = igl::copyleft::tetgen::tetrahedralize(V1, F1, "pq1e-2Y", Vtet, Ttet, Ftet);
	int np=Vtet.rows();
	int nt=Ttet.rows();
	Etet=TetEdgesFEM(Ttet);
	Node nodes[np];
	Tetrahedron  tet[nt];
	systemFEM sss(tet,nodes,nt,np);
	IntegratorFEM Integ;
	Integ.pS= &sss;

		for(int i=0; i<np; i++)
			{ Vector3d pos,pos1;
			  pos= Vtet.block<1,3>(i, 0) ;
			  Vector3d vel;
			  vel<<0 , 0 , 0;

			  nodes[i].setInitialPosition(pos);
			  pos1=pos;
			  nodes[i].setPosition(pos1);
			  nodes[i].setVelocity(vel);
			};

	for(int i=0; i<nt; i++)
			{
				tet[i].NodesIndex=Ttet.row(i);
				cout<<tet[i].NodesIndex<<'h'<<endl;
				tet[i].nodes=nodes;
				tet[i].setVolume();
				tet[i].preComputeB();
				tet[i].setDeformationGradient();
			};

	sss.setMassUnlumped();
	sss.setPositions();
	sss.setVelocities();
	sss.setForces();

	MeshTri3D mesh;
	mesh.setName("test mesh");
	mesh.updatePointBuffer(V1);
	mesh.addEdgeLayer("Edges")->updateEdgeBuffer(Etet);
	mesh.addFaceLayer("Faces")->updateFaceBuffer(F1);


	VectorXd a;
	a= sss.getPositions();
	a=sss.getForces();
	MatrixXd MM;
	mesh.initRenderable();

	Matrix3d StressTensor;
	StressTensor<< -1,0,0,
								 0,0,0,
								 0,0,0;
	VectorXd displacement= HarmonicDisplacement(Ftet,1e-1*StressTensor, sss);
	while(!window.shouldClose())
    {
    	A= sss.getPositions();
			A+=displacement;
    	A.resize(3,np);
    	V1=A.transpose();

        mesh.updatePointBuffer(V1);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    }

}

void mytest7()
{
	Window window("Visualizer");
	window.setSize(1200, 700);

	DenseMatrixX V1,V,A; // Vertices
	DenseMatrixXi E1, F1,Esort; // Edges, Faces

	utils::readOBJ("meshes/beam.obj", V1, E1, F1);


	DenseMatrixX Vtet,I; // Vertices
	DenseMatrixXi Ttet, Etet, Ftet; // Edges, Faces

	int status = igl::copyleft::tetgen::tetrahedralize(V1, F1, "pq1e-2Y", Vtet, Ttet, Ftet);
	Etet=TetEdgesFEM(Ttet);
	int np=Vtet.rows();
	int nt=Ttet.rows();

	Node nodes[np];
	Tetrahedron  tet[nt];
	systemFEM sss(tet,nodes,nt,np);
	IntegratorFEM Integ;
	Integ.pS= &sss;

	cout<<np<<"nodes"<<endl;
	cout<<nt<<"tets"<<endl;

	for(int i=0; i<np; i++)
			{ Vector3d pos,pos1;
			  pos= Vtet.block<1,3>(i, 0) ;
			  Vector3d vel;
			  vel<<0 , 0 , 0;

			  nodes[i].setInitialPosition(pos);
			  pos1=pos;
			  nodes[i].setPosition(pos1);
			  nodes[i].setVelocity(vel);
			};

	cout<<Ttet<<endl;

	for(int i=0; i<nt; i++)
			{
				tet[i].NodesIndex=Ttet.row(i);
				cout<<tet[i].NodesIndex<<'h'<<endl;
				tet[i].nodes=nodes;
				tet[i].setVolume();
				tet[i].preComputeB();
				tet[i].setDeformationGradient();
			};

	sss.setMassUnlumped();
	cout<<sss.getMass()<<endl;
	sss.setPositions();
	sss.setVelocities();
	sss.setForces();

	MeshTri3D mesh;
	mesh.setName("test mesh");
	mesh.updatePointBuffer(V1);
	mesh.addEdgeLayer("Edges")->updateEdgeBuffer(Etet);
	mesh.addFaceLayer("Faces")->updateFaceBuffer(F1);

	VectorXd a;
	a= sss.getPositions();
	a=sss.getForces();
	MatrixXd MM;
	mesh.initRenderable();
	Vector3d g;
	g<<0,0 ,-1;
	vector<bool> v_isFixed;
	cout<<V1<<endl;
	 for (int i=0; i<V1.rows();i++)
	 {
		 if (abs(V1(i,1)+2)<0.01)
		 		   {v_isFixed.push_back(true);
						v_isFixed.push_back(true);
						v_isFixed.push_back(true);
						cout<<"true"<<endl;
						}
						
		else 
					{v_isFixed.push_back(false); 
					 v_isFixed.push_back(false); 
					 v_isFixed.push_back(false); 
					 cout<<"false"<<endl;
					};
						
				  
	 }
	while(!window.shouldClose())
    {

		Integ.doStepImplicit(g,v_isFixed);
    	A= sss.getPositions();
    	A.resize(3,np);
    	V1=A.transpose();

        mesh.updatePointBuffer(V1);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    };
}
};
