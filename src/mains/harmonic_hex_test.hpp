#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>
#include <mains/FEM/hexahedron.hpp>
#include <mains/FEM/force_hex.hpp>
#include <mains/FEM/system_hex.hpp>
#include <mains/FEM/Integrator_fem.hpp>
#include <utils/hexahedronMesh.hpp>
#include <utils/ReadAndWriteEigenMatrix.hpp>
using namespace std;


namespace Mains
{

void HarmonicDisplacement(VectorXd* displacement,HexahedronCuboidMesh hmesh, Matrix3d StressTensor, systemFEM sF)
{
	int n_nodes=sF.n_nodes;
	MatrixXd Jacobian= sF.getForceJacobian();
	VectorXd LoadingForce;
	LoadingForce=VectorXd::Zero(3*n_nodes);
    MatrixXi Fhex= hmesh.getSurfaces();
    MatrixXd _nM= hmesh.getSurfaceNormalVector();
	for (int i=0; i<Fhex.rows(); i++)
	{
		Vector4i INDFaceNodes=Fhex.row(i);

        
	    Vector3d f;
        f<<_nM.row(i);
        f= StressTensor* f;
       
		if(f.norm()>1e-6)
		{
			LoadingForce.segment(3*INDFaceNodes(0),3)+=f/4;
			LoadingForce.segment(3*INDFaceNodes(1),3)+=f/4;
			LoadingForce.segment(3*INDFaceNodes(2),3)+=f/4;
            LoadingForce.segment(3*INDFaceNodes(3),3)+=f/4;
		};	
        
	};
    
	VectorXd _displacement;
  
	_displacement= -Jacobian.completeOrthogonalDecomposition().pseudoInverse()* LoadingForce;
    cout<<_displacement<<endl;
    *displacement=_displacement;
    cout<<"here"<<endl;
};

void HarmonicTest()
{
	Window window("Visualizer");
	window.setSize(1200, 700);
    HexahedronCuboidMesh a(3,3 ,3 , 1); 
    Node nodes[a.N_nodes];


    for(int i=0; i<a.N_nodes;i++)
        {   
            Vector3d pos,vel;
            vel<< 0,0,0;
            pos=a.getNodesPositions().segment(3*i,3);
            nodes[i].setInitialPosition(pos);
            nodes[i].setPosition(pos);
            nodes[i].setVelocity(vel);
        };
    
    Hexahedron hex[a.N_hex];
    MatrixXi nodesIndex= a.getHexahedrons();

    for(int i=0; i<a.N_hex; i++)
    {
        hex[i].nodes=nodes;
        hex[i].NodesIndex=nodesIndex.row(i);
        hex[i].preComputeB();
        hex[i].setDeformationGradient();
        hex[i].setVolume();
    }
    for(int i=0; i<a.N_hex; i++)
    {   
            double Y=5e4;
	        double ni=0.45;
            hex[i].miu= Y/(2*(1+ni));
	        hex[i].lambda=Y*ni/((1-2*ni)*(1+ni));
    } 

    //Only for 9*9 mesh
    /*
    for (int j=1; j<3; j++)
    {
        for(int i=(4*j-3)*(a.ny-1)*(a.nz-1); i<(4*j-1)*(a.ny-1)*(a.nz-1); i++)
        {   
            double Y=1e3;
	        double ni=0.45;
            hex[i].miu= Y/(2*(1+ni));
	        hex[i].lambda=Y*ni/((1-2*ni)*(1+ni));
        }
    }
    */
    

    //for 3*3 mesh
    /*
    double Y=1e3;
	double ni=0.45;
    hex[0].miu= Y/(2*(1+ni));
    hex[0].lambda=Y*ni/((1-2*ni)*(1+ni));
    */


    MatrixXd A= a.getNodesPositions();
    MatrixXd D=a.getInnerProductOfDerivative();
    Eigen::write_binary("InnerProduct.dat",D);

    A.resize(3,a.N_nodes);
    MatrixXd V1=A.transpose();
    MeshTri3D mesh;
	mesh.setName("test mesh");
	mesh.updatePointBuffer(V1);
	mesh.addEdgeLayer("Edges")->updateEdgeBuffer(a.getEdges());

    systemFEM sysHex(hex,nodes,a.N_hex,a.N_nodes);
    sysHex.initialize();

	mesh.initRenderable();

	Matrix3d StressTensor[6];
	StressTensor[0]<<               1,0,0,
								    0,0,0,
								    0,0,0;

    StressTensor[1]<<               0,1,0,
								    1,0,0,
								    0,0,0;

    StressTensor[2]<<               0,0,1,
								    0,0,0,
								    1,0,0;  

    StressTensor[3]<<               0,0,0,
								    0,1,0,
								    0,0,0;
                                    
    StressTensor[4]<<               0,0,0,
								    0,0,1,
								    0,1,0;

    StressTensor[5]<<               0,0,0,
								    0,0,0,
								    0,0,1;                                                                                               
    
    char FileName[6][10]={"h00.dat","h01.dat","h02.dat","h11.dat","h12.dat","h22.dat"};
	VectorXd displacement;
    int n_nodes=sysHex.n_nodes;
	MatrixXd Jacobian= sysHex.getForceJacobian();
    MatrixXi Fhex= a.getSurfaces();
    MatrixXd _nM= a.getSurfaceNormalVector();

	VectorXd LoadingForce;
    for(int j=0; j<6; j++)
    {
        LoadingForce=VectorXd::Zero(3*n_nodes);
       

        for (int i=0; i<Fhex.rows(); i++)
        {
            Vector4i INDFaceNodes=Fhex.row(i);            
            Vector3d f;
            f= StressTensor[j]* _nM.row(i).transpose();            
        
            if(f.norm()>1e-6)
            {
                LoadingForce.segment(3*INDFaceNodes(0),3)+=f/4;
                LoadingForce.segment(3*INDFaceNodes(1),3)+=f/4;
                LoadingForce.segment(3*INDFaceNodes(2),3)+=f/4;
                LoadingForce.segment(3*INDFaceNodes(3),3)+=f/4;
            };	
            
        };

        displacement= Jacobian.completeOrthogonalDecomposition().pseudoInverse()
                    * LoadingForce;
        MatrixXd C;
        C=displacement;
        C.resize(3,a.N_nodes);
        Eigen::write_binary(FileName[j],C.transpose());            
        

    }
	

	
    
	

    
    
    MatrixXd B;
    B= a.getNodesPositions();
	B+=1e4*displacement;
    B.resize(3,a.N_nodes);
    
    
    V1=B.transpose();
    
    while(!window.shouldClose())
    {

        mesh.updatePointBuffer(V1);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    };
    

    

}
};
