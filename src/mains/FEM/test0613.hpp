
using namespace std;

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>
#include <mains/FEM/hexahedron.hpp>
#include <mains/FEM/force_hex.hpp>
#include <mains/FEM/system_hex.hpp>
#include <mains/FEM/Integrator_fem.hpp>
#include <utils/hexahedronMesh.hpp>
#include <utils/SparseMatrixSlicing.hpp>
#include <vector>

#include <Eigen/SparseExtra>


#include <cstdio>
#include <ctime>
namespace Mains
{
void meshtest18()
{   
    HexahedronCuboidMesh a(28,28,28, 0.1);
    Node nodes[a.N_nodes];

    for(int i=0; i<a.N_nodes;i++)
        {   
            Vector3d pos,vel;
            vel<< 0,0,0;
            pos=a.getNodesPositions().segment(3*i,3);
            nodes[i].setInitialPosition(pos);
            nodes[i].setPosition(pos);
            nodes[i].setVelocity(vel);
        };
    
    cout<<"here1"<<endl;
    vector<Hexahedron*> hex;
    cout<<"here1.1"<<endl;

    MatrixXi nodesIndex= a.getHexahedrons();
    cout<<"here1.2"<<endl;
    for(int i=0; i<a.N_hex; i++)
    {   
        hex.push_back(new Hexahedron());
        cout<<"here1.2"<<endl;
        hex[i]->nodes=nodes;
        cout<<"here1.20"<<endl;
        hex[i]->NodesIndex=nodesIndex.row(i);
        cout<<"here1.21"<<endl;
        hex[i]->preComputeB();
        cout<<"here1.22"<<endl;
        hex[i]->setDeformationGradient();
        cout<<"here1.23"<<endl;
        hex[i]->setVolume();
        cout<<"here1.24"<<endl;
	double Y=5e6;
 	double ni=0.49;

	if (i%5==1) 
    {
        Y=1e3;
        ni=0.3;
    }else if (i%5==2)
    {
         Y=1e3;
        ni=0.49;
    }else if (i%5==3)
    {
         Y=1e3;
        ni=0.40;
    }else if (i%5==4)
    {
         Y=1e4;
        ni=0.30;
    };
    
	
        hex[i]->miu= Y/(2*(1+ni));
	hex[i]->lambda=Y*ni/((1-2*ni)*(1+ni));
	cout<<"here1.3"<<endl;
    }
    

    systemFEM sysHex(hex,nodes,a.N_hex,a.N_nodes);
    cout<<"here3"<<endl;
    sysHex.initialize();
   
    IntegratorFEM Integ;
    Integ.pS= &sysHex;
    Vector3d gravity;
    gravity<<0,-10,0;

    cout<<"here4"<<endl;
    vector<int> surface_node, xminus, xplus, yminus, yplus, zminus, zplus;
    VectorXd AllNodesInterpolated(3*a.N_nodes);
    double d= 1.0;

    int i=0;
    for(int j=0;j<a.ny;j++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            xminus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    i=a.nx-1;
    for(int j=0;j<a.ny;j++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            xplus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    int j=0;

    for(int i=0;i<a.nx;i++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            yminus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    j=a.ny-1;

    for(int i=0;i<a.nx;i++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            yplus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }


    int k=0;

    for(int i=0;i<a.nx;i++)
    {
        for(int j=0;j<a.ny;j++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            zminus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    k=a.nz-1;

    for(int i=0;i<a.nx;i++)
    {
        for(int j=0;j<a.ny;j++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            zplus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    cout<<surface_node.size()<<endl;
    
    sort( surface_node.begin(), surface_node.end() );
    surface_node.erase( unique( surface_node.begin(), surface_node.end() ), surface_node.end() );
    cout<<surface_node.size()<<endl;
    vector<int> vDOF_fix;
    
    for(int j=0;j<surface_node.size();j++)
    {
        for(int i=0;i<3;i++)
        {
            vDOF_fix.push_back(surface_node[j]*3+i);
        }
    }

    for(int k=0;k<vDOF_fix.size();k++)
    {
        cout<<vDOF_fix[k]<<endl;
    }
    vector<int> vDOF_free;
    j=0;
    for(int i=0; i<3*a.nx*a.ny*a.nz; i++)
    {
        if (i==vDOF_fix[j])
            j++;
        else
            vDOF_free.push_back(i);                   
    }
    
    cout<<"vDOF_free size"<<vDOF_free.size()<<endl;


    SpMat Hessian=sysHex.getForceJacobian();
    VectorXd aF=VectorXd::Constant(Hessian.cols(),1);
  
    // MatrixXd DenseHessian=MatrixXd(Hessian);
    // MatrixXd DenseH2=DenseHessian(vDOF_free,vDOF_fix);
    // 
    // MatrixXd DenseH1=DenseHessian(vDOF_free,vDOF_free);
    //auto a_fix=0.1*VectorXd::Random(vDOF_fix.size());
    auto a_fix=AllNodesInterpolated(vDOF_fix);

   // SpMat H2= MatrixSlicingToDense(vDOF_free,vDOF_fix,Hessian);
    
    SpMat H2= MatrixSlicing(vDOF_free,vDOF_fix,Hessian);
    //cout<<"H2-H2R \n" << H2-H2R<<endl;
    //cout<<"H2R \n" << H2R<<endl;
    //cout<<"here7.1"<<endl;
    auto RHS= H2* a_fix;
   
    SpMat H1= MatrixSlicing(vDOF_free,vDOF_free,Hessian);
 
    std::clock_t start;
    double duration;

    start = std::clock();


    Eigen::SimplicialCholesky<SpMat> chol(-H1);

    auto cputime=std::clock() - start;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"Cholesky: the total time for the factorization:  "<< duration<< " seconds" <<"   CPU time:  "<< cputime <<'\n';

    start = std::clock();

    VectorXd c= chol.solve(RHS);

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"the total time for the  backsubstitution:  "<< duration<< " seconds" <<'\n';
    

    /////////////////////////////////conjugate gradient

    ConjugateGradient<SparseMatrix<double>, Lower|Upper> cg;
    
    start = std::clock();

    cg.compute(-H1);

    cputime=std::clock() - start;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"CG: the total time for the preprocessing:  "<< duration<< " seconds" <<"   CPU time:  "<< cputime <<'\n';

    start = std::clock();
    VectorXd x = cg.solve(RHS);
     duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"the total time for the CG solve:  "<< duration<< " seconds" <<'\n';
    std::cout << "#iterations:     " << cg.iterations() << std::endl;
    std::cout << "#estimated error: " << cg.error()      << std::endl;
}
void Jesus1()
{   
    HexahedronCuboidMesh a(28,28,28, 1);
    Node nodes[a.N_nodes];

    for(int i=0; i<a.N_nodes;i++)
        {   
            Vector3d pos,vel;
            vel<< 0,0,0;
            pos=a.getNodesPositions().segment(3*i,3);
            nodes[i].setInitialPosition(pos);
            nodes[i].setPosition(pos);
            nodes[i].setVelocity(vel);
        };
    
    cout<<"here1"<<endl;
    vector<Hexahedron*> hex;
    cout<<"here1.1"<<endl;

    MatrixXi nodesIndex= a.getHexahedrons();
    cout<<"here1.2"<<endl;
    for(int i=0; i<a.N_hex; i++)
    {   
        hex.push_back(new Hexahedron());
        cout<<"here1.2"<<endl;
        hex[i]->nodes=nodes;
        cout<<"here1.20"<<endl;
        hex[i]->NodesIndex=nodesIndex.row(i);
        cout<<"here1.21"<<endl;
        hex[i]->preComputeB();
        cout<<"here1.22"<<endl;
        hex[i]->setDeformationGradient();
        cout<<"here1.23"<<endl;
        hex[i]->setVolume();
        cout<<"here1.24"<<endl;
	double Y=5e4;
 	double ni=0.4999;

	if (i%2==0) 
    {
        Y=1e3;
        ni=0.4;
    }
	
        hex[i]->miu= Y/(2*(1+ni));
	hex[i]->lambda=Y*ni/((1-2*ni)*(1+ni));
	cout<<"here1.3"<<endl;
    }
    

    systemFEM sysHex(hex,nodes,a.N_hex,a.N_nodes);
    cout<<"here3"<<endl;
    sysHex.initialize();
   
    IntegratorFEM Integ;
    Integ.pS= &sysHex;
    Vector3d gravity;
    gravity<<0,-10,0;

    cout<<"here4"<<endl;
    vector<int> surface_node, xminus, xplus, yminus, yplus, zminus, zplus;
    VectorXd AllNodesInterpolated(3*a.N_nodes);
    double d= 10.0;

    int i=0;
    for(int j=0;j<a.ny;j++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            xminus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    i=a.nx-1;
    for(int j=0;j<a.ny;j++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            xplus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    int j=0;

    for(int i=0;i<a.nx;i++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            yminus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    j=a.ny-1;

    for(int i=0;i<a.nx;i++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            yplus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }


    int k=0;

    for(int i=0;i<a.nx;i++)
    {
        for(int j=0;j<a.ny;j++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            zminus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    k=a.nz-1;

    for(int i=0;i<a.nx;i++)
    {
        for(int j=0;j<a.ny;j++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
            zplus.push_back(a.ny*a.nz*i+a.nz*j+k);
            AllNodesInterpolated(3*(a.ny*a.nz*i+a.nz*j+k)+1)= j*2*d/(a.nz-1)-d;
        }
    }

    cout<<surface_node.size()<<endl;
    
    sort( surface_node.begin(), surface_node.end() );
    surface_node.erase( unique( surface_node.begin(), surface_node.end() ), surface_node.end() );
    cout<<surface_node.size()<<endl;
    vector<int> vDOF_fix;
    
    for(int j=0;j<surface_node.size();j++)
    {
        for(int i=0;i<3;i++)
        {
            vDOF_fix.push_back(surface_node[j]*3+i);
        }
    }

    for(int k=0;k<vDOF_fix.size();k++)
    {
        cout<<vDOF_fix[k]<<endl;
    }
    vector<int> vDOF_free;
    j=0;
    for(int i=0; i<3*a.nx*a.ny*a.nz; i++)
    {
        if (i==vDOF_fix[j])
            j++;
        else
            vDOF_free.push_back(i);                   
    }
    
    cout<<"vDOF_free size"<<vDOF_free.size()<<endl;


    SpMat Hessian;
    loadMarket(Hessian,"grid_2ordersYoung_1orderPoisson.csv");
    Hessian.selfadjointView<Lower>();
    cout<<"Hessian size:"<<Hessian.size()<<endl;
    VectorXd aF=VectorXd::Constant(Hessian.cols(),1);
  
    // MatrixXd DenseHessian=MatrixXd(Hessian);
    // MatrixXd DenseH2=DenseHessian(vDOF_free,vDOF_fix);
    // 
    // MatrixXd DenseH1=DenseHessian(vDOF_free,vDOF_free);
    //auto a_fix=0.1*VectorXd::Random(vDOF_fix.size());
    auto a_fix=AllNodesInterpolated(vDOF_fix);

   // SpMat H2= MatrixSlicingToDense(vDOF_free,vDOF_fix,Hessian);
    
    SpMat H2= MatrixSlicing(vDOF_free,vDOF_fix,Hessian);
    //cout<<"H2-H2R \n" << H2-H2R<<endl;
    //cout<<"H2R \n" << H2R<<endl;
    //cout<<"here7.1"<<endl;
    cout<<a_fix<<endl;
    auto RHS= H2* a_fix;
    //auto RHS= VectorXd::Random(vDOF_free.size());
    //cout<<"RHS"<<RHS<<endl;
   
    SpMat H1= MatrixSlicing(vDOF_free,vDOF_free,Hessian);
 
    std::clock_t start;
    double duration;

    start = std::clock();


    Eigen::SimplicialCholesky<SpMat> chol(-H1);

    auto cputime=std::clock() - start;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"Cholesky: the total time for the factorization:  "<< duration<< " seconds" <<"   CPU time:  "<< cputime <<'\n';

    start = std::clock();

    VectorXd c= chol.solve(RHS);

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"the total time for the  backsubstitution:  "<< duration<< " seconds" <<'\n';
    

    /////////////////////////////////conjugate gradient

    ConjugateGradient<SparseMatrix<double>, Lower|Upper> cg;
    
    start = std::clock();

    cg.compute(-H1);

    cputime=std::clock() - start;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"CG: the total time for the preprocessing:  "<< duration<< " seconds" <<"   CPU time:  "<< cputime <<'\n';

    start = std::clock();
    VectorXd x = cg.solve(RHS);
     duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"the total time for the CG solve:  "<< duration<< " seconds" <<'\n';
    std::cout << "#iterations:     " << cg.iterations() << std::endl;
    std::cout << "#estimated error: " << cg.error()      << std::endl;
}
void meshtest()
{   
    HexahedronCuboidMesh a(7,7,7, 0.1);
    Node nodes[a.N_nodes];

    for(int i=0; i<a.N_nodes;i++)
        {   
            Vector3d pos,vel;
            vel<< 0,0,0;
            pos=a.getNodesPositions().segment(3*i,3);
            nodes[i].setInitialPosition(pos);
            nodes[i].setPosition(pos);
            nodes[i].setVelocity(vel);
        };
    
    cout<<"here1"<<endl;
    vector<Hexahedron*> hex;
    cout<<"here1.1"<<endl;

    MatrixXi nodesIndex= a.getHexahedrons();
    cout<<"here1.2"<<endl;
    for(int i=0; i<a.N_hex; i++)
    {   
        hex.push_back(new Hexahedron());
        cout<<"here1.2"<<endl;
        hex[i]->nodes=nodes;
        cout<<"here1.20"<<endl;
        hex[i]->NodesIndex=nodesIndex.row(i);
        cout<<"here1.21"<<endl;
        hex[i]->preComputeB();
        cout<<"here1.22"<<endl;
        hex[i]->setDeformationGradient();
        cout<<"here1.23"<<endl;
        hex[i]->setVolume();
        cout<<"here1.24"<<endl;
	double Y=8e5;
	double ni=0.48;
        hex[i]->miu= Y/(2*(1+ni));
	hex[i]->lambda=Y*ni/((1-2*ni)*(1+ni));
	cout<<"here1.3"<<endl;
    }
    

    systemFEM sysHex(hex,nodes,a.N_hex,a.N_nodes);
    cout<<"here3"<<endl;
    sysHex.initialize();
   
    IntegratorFEM Integ;
    Integ.pS= &sysHex;
    Vector3d gravity;
    gravity<<0,-10,0;

    cout<<"here4"<<endl;
    vector<int> surface_node;
    int i=0;
    for(int j=0;j<a.ny;j++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
        }
    }

    i=a.nx-1;
    for(int j=0;j<a.ny;j++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
        }
    }

    int j=0;

    for(int i=0;i<a.nx;i++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
        }
    }

    j=a.ny-1;

    for(int i=0;i<a.nx;i++)
    {
        for(int k=0;k<a.nz;k++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
        }
    }


    int k=0;

    for(int i=0;i<a.nx;i++)
    {
        for(int j=0;j<a.ny;j++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
        }
    }

    k=a.nz-1;

    for(int i=0;i<a.nx;i++)
    {
        for(int j=0;j<a.ny;j++)
        {
            surface_node.push_back(a.ny*a.nz*i+a.nz*j+k);
        }
    }

    cout<<surface_node.size()<<endl;
    
    sort( surface_node.begin(), surface_node.end() );
    surface_node.erase( unique( surface_node.begin(), surface_node.end() ), surface_node.end() );
    cout<<surface_node.size()<<endl;
    vector<int> vDOF_fix;
    
    for(int j=0;j<surface_node.size();j++)
    {
        for(int i=0;i<3;i++)
        {
            vDOF_fix.push_back(surface_node[j]*3+i);
        }
    }

    for(int k=0;k<vDOF_fix.size();k++)
    {
        cout<<vDOF_fix[k]<<endl;
    }
    vector<int> vDOF_free;
    j=0;
    for(int i=0; i<3*a.nx*a.ny*a.nz; i++)
    {
        if (i==vDOF_fix[j])
            j++;
        else
            vDOF_free.push_back(i);                   
    }
    
    cout<<"vDOF_free size"<<vDOF_free.size()<<endl;

    for(int i=0; i<vDOF_free.size(); i++)
    {
        cout<<vDOF_free[i]<<endl;
    }

    SpMat Hessian=sysHex.getForceJacobian();
    VectorXd aF=VectorXd::Random(Hessian.cols());
    cout<<"here5"<<endl;
    MatrixXd I_fix(vDOF_fix.size(),vDOF_fix.size());
    I_fix.setIdentity();
    MatrixXd DenseHessian=MatrixXd(Hessian);
    DenseHessian(vDOF_fix,vDOF_fix)+=1000*I_fix;
    VectorXd RHS= aF;
    auto MHessian= DenseHessian.sparseView();
    std::clock_t start;
    double duration;

    start = std::clock();

   
    Eigen::SimplicialCholesky<SpMat> chol(MHessian);

    auto cputime=std::clock() - start;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"Cholesky: the total time for the factorization:  "<< duration<< " seconds" <<"   CPU time:  "<< cputime <<'\n';

    start = std::clock();

    VectorXd c= chol.solve(RHS);

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"the total time for the  backsubstitution:  "<< duration<< " seconds" <<'\n';

       /////////////////////////////////conjugate gradient

    ConjugateGradient<SparseMatrix<double>, Lower|Upper> cg;
    
    start = std::clock();

    cg.compute(MHessian);

    cputime=std::clock() - start;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"CG: the total time for the preprocessing:  "<< duration<< " seconds" <<"   CPU time:  "<< cputime <<'\n';

    start = std::clock();
    VectorXd x = cg.solve(RHS);
     duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"the total time for the CG solve:  "<< duration<< " seconds" <<'\n';
    std::cout << "#iterations:     " << cg.iterations() << std::endl;
    std::cout << "#estimated error: " << cg.error()      << std::endl;
    
}
void SparseMatrixTest()
{   
    SpMat S(2,2);
    std::vector<T> tripletList;
    tripletList.push_back(T(0,0,1));
    vector<int> v1;
    vector<int> v2;
    v1.push_back(1);
    v2.push_back(0);
    v2.push_back(1);
    S.setFromTriplets(tripletList.begin(), tripletList.end());
    auto Ss= MatrixSlicing(v1, v2, S);
    cout<<Ss<<endl;
}

}
