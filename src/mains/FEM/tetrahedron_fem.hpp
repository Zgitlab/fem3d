#ifndef TETRAHEDRON_H_
#define TETRAHEDRON_H_

#include <mains/FEM/node_fem.hpp>
class Tetrahedron {
public:
	void preComputeB();//precomputing necessary matrix for deformation gradient
	void setDeformationGradient();
	Matrix3d getDeformationGradient();
	MatrixXd getBlockB();
	
	void setVolume();
	double getVolume();
	double getMass();

	Node* nodes;
	Vector4i NodesIndex;


private:
	Matrix3d DeformationGradient;
	MatrixXd B;//precomputed matrix for deformation gradient
	MatrixXd BlockB;
	double volume=0;
	double rho=1; //density
};

void Tetrahedron::preComputeB()
{
	MatrixXd G(4,3);
	G<< -1,-1,-1,
		 1, 0, 0,
		 0, 1, 0,
		 0, 0, 1;
	MatrixXd N0(3,4);
	N0<<  nodes[NodesIndex(0)].getInitialPosition(),
		  nodes[NodesIndex(1)].getInitialPosition(),
		  nodes[NodesIndex(2)].getInitialPosition(),
		  nodes[NodesIndex(3)].getInitialPosition();//N0 is transposed version of the slides;

	B=G*((N0*G).inverse());
	MatrixXd _blockB(9,12);
	_blockB<<	B(0,0),0,0,B(1,0),0,0,B(2,0),0,0,B(3,0),0,0,
				0,B(0,0),0,0,B(1,0),0,0,B(2,0),0,0,B(3,0),0,
				0,0,B(0,0),0,0,B(1,0),0,0,B(2,0),0,0,B(3,0),
				B(0,1),0,0,B(1,1),0,0,B(2,1),0,0,B(3,1),0,0,
				0,B(0,1),0,0,B(1,1),0,0,B(2,1),0,0,B(3,1),0,
				0,0,B(0,1),0,0,B(1,1),0,0,B(2,1),0,0,B(3,1),
				B(0,2),0,0,B(1,2),0,0,B(2,2),0,0,B(3,2),0,0,
				0,B(0,2),0,0,B(1,2),0,0,B(2,2),0,0,B(3,2),0,
				0,0,B(0,2),0,0,B(1,2),0,0,B(2,2),0,0,B(3,2);

	BlockB=_blockB;

}

void Tetrahedron::setDeformationGradient()
{
	MatrixXd Nx(3,4);
	Nx<<nodes[NodesIndex(0)].getPosition(),
		nodes[NodesIndex(1)].getPosition(),
		nodes[NodesIndex(2)].getPosition(),
		nodes[NodesIndex(3)].getPosition();
	DeformationGradient=Nx*B;


}

Matrix3d Tetrahedron::getDeformationGradient()
{
	return DeformationGradient;
}

void Tetrahedron::setVolume()
{
	MatrixXd _T(3,3);
	MatrixXd _Tnew(3,3);
	double _volume;
	_T<< nodes[NodesIndex(1)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
		 nodes[NodesIndex(2)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
		 nodes[NodesIndex(3)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition();

	_volume=(_T.determinant()/6);

	if (_volume<0)
		{
		int _n0=NodesIndex(0);
		int _n3=NodesIndex(3);
		NodesIndex(0)=_n3;
		NodesIndex(3)=_n0;

		_Tnew<<  nodes[NodesIndex(1)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
				 nodes[NodesIndex(2)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
				 nodes[NodesIndex(3)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition();

		_volume=(_Tnew.determinant()/6);
		};

	volume=_volume;
}
double Tetrahedron::getVolume()
{
	return volume;
	}
double Tetrahedron::getMass()
{
	return rho*volume;
	}
MatrixXd Tetrahedron::getBlockB()
{
	return BlockB;
}

#endif /* TETRAHEDRON_H_ */
