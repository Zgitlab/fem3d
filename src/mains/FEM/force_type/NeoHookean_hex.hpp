/*
 * NeoHookean.hpp
 *
 *  Created on: May 8, 2019
 *      Author: zhongyun
 */

#ifndef SRC_MAINS_FEM_FORCE_TYPE_NEOHOOKEAN_HPP_
#define SRC_MAINS_FEM_FORCE_TYPE_NEOHOOKEAN_HPP_


#include <mains/FEM/force_hex.hpp>

class NeoHookeanForce: public LocalForce{
	public:
	NeoHookeanForce(Hexahedron* ptr);
	void setPiolaTensor();
	void setPiolaVector();
	void setDPDF();
	void setInternalPotentialEnergy();
	void setForce();

	void setForceJacobian();
	void setAll();
	double miu=1;
	double lambda=1;
	double Y=8e6;
	double ni=0.48;
	MatrixXd dPdF[9];
	Matrix3d crossMatrix(Vector3d v)
    {	Matrix3d m;
        m <<   0, -v(2),  v(1),
            v(2),     0, -v(0),			
           -v(1),  v(0),    0;
        return m;
    };

};

NeoHookeanForce::NeoHookeanForce(Hexahedron* ptr)
{
	ptr_hex=ptr;
	if (((ptr->lambda)<0)&&((ptr->miu)<0))
	{	miu= Y/(2*(1+ni));
		lambda=Y*ni/((1-2*ni)*(1+ni));
	}
	else 
	{
		miu=ptr->miu;
		lambda=ptr->lambda;

	};

};

void NeoHookeanForce::setPiolaTensor()
{
	Matrix3d* F=ptr_hex->getDeformationGradient();
	
	for(int i=0;i<8;i++)
	{
	double J= F[i].determinant();
	Matrix3d Ftrans=F[i].transpose();
	Matrix3d Finv_trans=Ftrans.inverse();	
	PiolaTensor[i]=miu*F[i]-miu*Finv_trans+lambda*log(J)*Finv_trans;//example sifakis 23
	};

}

void NeoHookeanForce::setPiolaVector()
{
	for (int i=0; i<8; i++)
	{
	Matrix3d P;
	P= PiolaTensor[i];
	Map<VectorXd> vP(P.data(), 9);
	PiolaVector[i]=vP;
	};
}

void NeoHookeanForce::setDPDF()
{
	Matrix3d* F= ptr_hex->getDeformationGradient();
	for (int i=0; i<8; i++)
	{
	MatrixXd _dPdF(9,9);
	VectorXd vecF(9);
	VectorXd dJdF(9);
	

	Vector3d f1=F[i].col(0);
	Vector3d f2=F[i].col(1);
	Vector3d f3=F[i].col(2);

	vecF  << f1, f2, f3;
	dJdF <<  f2.cross(f3),
			 f3.cross(f1),
			 f1.cross(f2);

	Matrix3d m0, m1, m2;
	MatrixXd ddJddF(9,9);
	MatrixXd I9(9,9);
	I9=MatrixXd::Identity(9, 9);

	m0=crossMatrix(F[i].col(0));
	m1=crossMatrix(F[i].col(1));
	m2=crossMatrix(F[i].col(2));
	ddJddF << Matrix3d::Zero(), -m2, m1,
	          m2, Matrix3d::Zero(), -m0,
	          -m1, m0, Matrix3d::Zero();

	double k1=0, k2=0, k3=0, k4=0;
	double J= F[i].determinant();


	k1+=miu;
	k3+=(miu+lambda-lambda*log(J))/(J*J);
	k4+=-miu/J +lambda*log(J)/J;

	_dPdF=k1*I9+
		  k2*vecF*vecF.transpose()+
		  k3*dJdF*dJdF.transpose()+
		  k4*ddJddF;

	dPdF[i]=_dPdF;
	};

	


}

void NeoHookeanForce::setForce()
{
	VectorXd _Force(24);
	_Force.setZero();
	MatrixXd* bB;
	for (int i=0; i<8; i++)
	{
	
	bB =ptr_hex->getBlockB();
	double _V =ptr_hex->getVolume();	
	_Force+= -_V*bB[i].transpose()*PiolaVector[i];	
	};
	Force=0.125*_Force;
}

void NeoHookeanForce::setForceJacobian()
{
	MatrixXd* bB;
	bB =ptr_hex->getBlockB();
	double _V =ptr_hex->getVolume();
	MatrixXd _ForceJacobian(24,24);
	_ForceJacobian.setZero();
	for (int i=0; i<8; i++)
	{
	_ForceJacobian+= -_V*bB[i].transpose()*dPdF[i]*bB[i];
	};
	ForceJacobian=0.125*_ForceJacobian;
}

void NeoHookeanForce::setAll()
{ setPiolaTensor();
  setPiolaVector();
  setDPDF();
  setForce();
  setForceJacobian();
}

#endif /* SRC_MAINS_FEM_FORCE_TYPE_NEOHOOKEAN_HPP_ */
