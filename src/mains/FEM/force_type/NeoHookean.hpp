/*
 * NeoHookean.hpp
 *
 *  Created on: May 8, 2019
 *      Author: zhongyun
 */

#ifndef SRC_MAINS_FEM_FORCE_TYPE_NEOHOOKEAN_HPP_
#define SRC_MAINS_FEM_FORCE_TYPE_NEOHOOKEAN_HPP_


#include <mains/FEM/force_fem.hpp>

class NeoHookeanForce: public LocalForce{
	public:
	NeoHookeanForce(Tetrahedron* ptr);
	void setPiolaTensor();
	void setPiolaVector();
	void setDPDF();
	void setInternalPotentialEnergy();
	void setForce();

	void setForceJacobian();
	void setAll();
	double miu=1;
	double lambda=1;
	double Y=1e10;
	double ni=0.48;

	Matrix3d crossMatrix(Vector3d v)
    {	Matrix3d m;
        m <<   0, -v(2),  v(1),
            v(2),     0, -v(0),
           -v(1),  v(0),    0;
        return m;
    };

};

NeoHookeanForce::NeoHookeanForce(Tetrahedron* ptr)
{
	ptr_tet=ptr;
	miu= Y/(2*(1+ni));
	cout<<miu<<"miu"<<endl;
	lambda=Y*ni/((1-2*ni)*(1+ni));
	cout<<lambda<<"lambda"<<endl;
};
void NeoHookeanForce::setInternalPotentialEnergy()
{
	Matrix3d F=ptr_tet->getDeformationGradient();
	double vol= ptr_tet->getVolume();

	double J= F.determinant();
	double I1=(F.transpose()*F).trace();
	double phi= 0.5*miu* (I1-3)-
				    miu*log(J)+
				0.5*lambda*log(J)*log(J);

	InternalPotentialEnergy=phi*vol;

}

void NeoHookeanForce::setPiolaTensor()
{
	Matrix3d F=ptr_tet->getDeformationGradient();
	double J= F.determinant();
	Matrix3d Ftrans=F.transpose();
	Matrix3d Finv_trans=Ftrans.inverse();

	Matrix3d P;
	P=miu*F-miu*Finv_trans+lambda*log(J)*Finv_trans;//example sifakis 23

	PiolaTensor=P;
}

void NeoHookeanForce::setPiolaVector()
{
	Matrix3d P;
	P= PiolaTensor;
	Map<VectorXd> vP(P.data(), 9);
	PiolaVector=vP;
}

void NeoHookeanForce::setDPDF()
{
	MatrixXd _dPdF(9,9);
	VectorXd vecF(9);
	VectorXd dJdF(9);
	MatrixXd F= ptr_tet->getDeformationGradient();

	Vector3d f1=F.col(0);
	Vector3d f2=F.col(1);
	Vector3d f3=F.col(2);

	vecF  << f1, f2, f3;
	dJdF <<  f2.cross(f3),
			 f3.cross(f1),
			 f1.cross(f2);

	Matrix3d m0, m1, m2;
	MatrixXd ddJddF(9,9);
	MatrixXd I9(9,9);
	I9=MatrixXd::Identity(9, 9);

	m0=crossMatrix(F.col(0));
	m1=crossMatrix(F.col(1));
	m2=crossMatrix(F.col(2));
	ddJddF << Matrix3d::Zero(), -m2, m1,
	          m2, Matrix3d::Zero(), -m0,
	          -m1, m0, Matrix3d::Zero();

	double k1=0, k2=0, k3=0, k4=0;
	double J= F.determinant();


	k1+=miu;
	k3+=(miu+lambda-lambda*log(J))/(J*J);
	k4+=-miu/J +lambda*log(J)/J;

	_dPdF=k1*I9+
		  k2*vecF*vecF.transpose()+
		  k3*dJdF*dJdF.transpose()+
		  k4*ddJddF;

	dPdF=_dPdF;


}

void NeoHookeanForce::setForce()
{
	MatrixXd bB;
	bB =ptr_tet->getBlockB();
	double _V =ptr_tet->getVolume();
	VectorXd _Force;
	_Force= -_V*bB.transpose()*PiolaVector;
	Force=_Force;
}

void NeoHookeanForce::setForceJacobian()
{
	MatrixXd bB;
	bB =ptr_tet->getBlockB();
	double _V =ptr_tet->getVolume();
	MatrixXd _ForceJacobian;
	_ForceJacobian= -_V*bB.transpose()*dPdF*bB;
	ForceJacobian=_ForceJacobian;
}

void NeoHookeanForce::setAll()
{ setPiolaTensor();
  setPiolaVector();
  setDPDF();
  setForce();
  setForceJacobian();
}

#endif /* SRC_MAINS_FEM_FORCE_TYPE_NEOHOOKEAN_HPP_ */
