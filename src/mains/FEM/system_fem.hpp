/*
 * system_fem.hpp
 *
 *  Created on: May 6, 2019
 *      Author: zhongyun
 */

#ifndef SRC_MAINS_FEM_SYSTEM_FEM_HPP_
#define SRC_MAINS_FEM_SYSTEM_FEM_HPP_


#include <Eigen/Sparse>
#include <iostream>
#include <mains/FEM/tetrahedron_fem.hpp>
#include <mains/FEM/force_fem.hpp>
#include <mains/FEM/force_type/NeoHookean.hpp>
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;
typedef double (*ExternalPotentialEnergy_funtion)(Node*);
using namespace std;
using namespace Eigen;

class systemFEM{
public:
	systemFEM(Tetrahedron* _tets,Node* _nodes,int n_tets,int n_n);
	Tetrahedron* ptr_tets;
	Node* ptr_nodes;

	int n_tetrahedrons;
	int n_nodes;



	VectorXd getPositions();
	void setPositions(VectorXd pos);
	void setPositions();

	VectorXd getVelocities();
	void setVelocities(VectorXd vel);
	void setVelocities();

	SpMat getMass();
	void setMass();
	void setMassUnlumped();
	VectorXd getForces();
	void setForces();//Jacobian is also set here

	SpMat getForceJacobian();

	void update();



private:
	VectorXd Forces;
	SpMat Mass;
	SpMat dFdX;
	SpMat MassInverse;
	VectorXd Positions;
	VectorXd Velocities;
	double InternalPotentialEnergy=0;
	double ExternalPotentialEnergy=0;

};

systemFEM::systemFEM(Tetrahedron* _tets,Node* _nodes,int n_tets,int n_n)
{
	ptr_tets=_tets;
	ptr_nodes=_nodes;
	n_tetrahedrons=n_tets;
	n_nodes=n_n;
};

VectorXd systemFEM::getPositions() {
	return Positions;
};

void systemFEM::setPositions(VectorXd pos) {
	 Positions=pos;
};

void systemFEM::setPositions() {

	 Positions=VectorXd::Zero(3*n_nodes);
     for(int i=0; i<n_nodes; i++)
     {
    	 Positions.segment(3*i,3)=ptr_nodes[i].getPosition();

     };

};

VectorXd systemFEM::getVelocities() {
	return Velocities;
};

void systemFEM::setVelocities(VectorXd vol) {
	 Velocities=vol;
};

void systemFEM::setVelocities() {
	 Velocities=VectorXd::Zero(3*n_nodes);
	 for(int i=0; i<n_nodes; i++)
	     {
	    	 Velocities.segment(3*i,3)=ptr_nodes[i].getVelocity();
	     };
};


SpMat systemFEM::getMass()
{
	return Mass;
}


void systemFEM::setMass()
{   SpMat mass(3*n_nodes,3*n_nodes);
	SpMat massInverse(3*n_nodes,3*n_nodes);
	std::vector<T> tripletList;
	std::vector<T> tripletListInverse;
	tripletList.reserve(4*n_tetrahedrons);

    for(int i=0; i<n_tetrahedrons; i++)
    {	double m=ptr_tets[i].getMass();

    	for(int k=0; k<4; k++)
    	{
    	int ind=ptr_tets[i].NodesIndex(k);
    	tripletList.push_back(T(3*ind,3*ind,m/4));
    	tripletList.push_back(T(3*ind+1,3*ind+1,m/4));
    	tripletList.push_back(T(3*ind+2,3*ind+2,m/4));
    	}

    };

    mass.setFromTriplets(tripletList.begin(), tripletList.end());
    Mass=mass;
}

void systemFEM::setMassUnlumped()
{   SpMat mass(3*n_nodes,3*n_nodes);
	std::vector<T> tripletList;
	std::vector<T> tripletListInverse;
	tripletList.reserve(36*n_tetrahedrons);

    for(int i=0; i<n_tetrahedrons; i++)
    {	double m=ptr_tets[i].getMass();

    	for(int j=0; j<4; j++)
    	{
    		for(int k=0; k<4; k++)
    		{	if(k==j)
    			{
    		  	int ind=ptr_tets[i].NodesIndex(k);
    		    tripletList.push_back(T(3*ind,3*ind,m/10));
    		    tripletList.push_back(T(3*ind+1,3*ind+1,m/10));
    		    tripletList.push_back(T(3*ind+2,3*ind+2,m/10));
    			}
    			else
    			{
    				int ind1=ptr_tets[i].NodesIndex(k);
    				int ind2=ptr_tets[i].NodesIndex(j);
    			    tripletList.push_back(T(3*ind1,3*ind2,m/20));
    			    tripletList.push_back(T(3*ind1+1,3*ind2+1,m/20));
    			    tripletList.push_back(T(3*ind1+2,3*ind2+2,m/20));
    			};
    		};
    	};
    };

    mass.setFromTriplets(tripletList.begin(), tripletList.end());
    Mass=mass;
}

VectorXd systemFEM::getForces()
{	setForces();
	return Forces;
}

void systemFEM::setForces()
{	VectorXd _Forces;
	_Forces=VectorXd::Zero(3*n_nodes);

	SpMat _dFdX(3*n_nodes,3*n_nodes);
	_dFdX.setZero();
	std::vector<T> tripletList;
	tripletList.reserve(144*n_tetrahedrons);

	for (int i=0; i<n_tetrahedrons; i++)
	{	
		
		NeoHookeanForce ObjF(&ptr_tets[i]);

		ObjF.setAll();
		MatrixXd J=ObjF.getForceJacobian();
		VectorXd _tetraForce;
		_tetraForce=ObjF.getForce();
		for(int k=0; k<4; k++)
		    	{
		    	int ind=ptr_tets[i].NodesIndex(k);
		    	_Forces.segment(3*ind,3) += _tetraForce.segment(3*k,3);
		    	}


		for(int j=0; j<J.rows();j++)
		{
			for(int k=0; k<J.cols();k++)
			{
			if (abs(J(j,k))>1.0e-30)
				{
				int IND1=ptr_tets[i].NodesIndex(j/3);
				int IND2=ptr_tets[i].NodesIndex(k/3);
				int RES1=j-3*(j/3);
				int RES2=k-3*(k/3);
				tripletList.push_back(T(3*IND1+RES1,3*IND2+RES2,J(j,k)));
				}

			};

		};
	};

	_dFdX.setFromTriplets(tripletList.begin(), tripletList.end());
	dFdX=_dFdX;
	Forces=_Forces;
}




SpMat systemFEM::getForceJacobian()
{
	return dFdX;
};


void systemFEM::update()
{
	for(int i=0; i<n_nodes; i++)
		     {
		    	 ptr_nodes[i].setPosition(Positions.segment(3*i,3));
		    	 ptr_nodes[i].setVelocity(Velocities.segment(3*i,3));
		     };

	for(int i=0; i<n_tetrahedrons; i++)
			 {
			 	 ptr_tets[i].setDeformationGradient();
			 };
}




#endif /* SRC_MAINS_FEM_SYSTEM_FEM_HPP_ */
