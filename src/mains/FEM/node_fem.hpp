#pragma once

#include <Eigen/Sparse>
#include <iostream>

using namespace std;
using namespace Eigen;



class Node {
public:
	Vector3d getPosition();
	void setPosition(Vector3d pos);

	Vector3d getInitialPosition();
	void setInitialPosition(Vector3d pos);

	Vector3d getVelocity();
	void setVelocity(Vector3d Vel);

private:
	Vector3d position;
	Vector3d velocity;
	Vector3d InitialPosition;

};

void Node::setPosition(Vector3d pos)
{
	position= pos;
};


Vector3d Node::getPosition()
{
	return position;
};


void Node::setInitialPosition(Vector3d pos)
{
	InitialPosition = pos;
};

Vector3d Node::getInitialPosition()
{
	return InitialPosition;
};



Vector3d Node::getVelocity()
{
	return velocity;
};


void Node::setVelocity(Vector3d vel)
{
	velocity=vel;
};

