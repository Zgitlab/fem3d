void mytest7(IntegratorFEM Integ)
{
	Window window("Visualizer");
	window.setSize(1200, 700);


	Integ.pS->initialize();


	MeshTri3D mesh;
	mesh.setName("test mesh");
	mesh.updatePointBuffer(V1);
	mesh.addEdgeLayer("Edges")->updateEdgeBuffer(Etet);
	mesh.addFaceLayer("Faces")->updateFaceBuffer(F1);

	VectorXd a;
	a= sss.getPositions();
	a=sss.getForces();
	MatrixXd MM;
	mesh.initRenderable();

	while(!window.shouldClose())
    {

		Integ.doStepImplicit();
    	A= sss.getPositions();
    	A.resize(3,np);
    	V1=A.transpose();

        mesh.updatePointBuffer(V1);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    };
}