/*
 * staticsolver_fem.hpp
 *
 *  Created on: May 14, 2019
 *      Author: zhongyun
 */

#ifndef SRC_MAINS_FEM_STATICSOLVER_FEM_HPP_
#define SRC_MAINS_FEM_STATICSOLVER_FEM_HPP_

#include <mains/FEM/system_fem.hpp>

class StaticSolver{
	systemFEM* pS;
	void LinearSingleSolve();
	void LineSearch();
};




#endif /* SRC_MAINS_FEM_STATICSOLVER_FEM_HPP_ */
