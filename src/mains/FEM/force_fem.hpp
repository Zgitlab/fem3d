/*
 * piola_tensor.hpp
 *
 *  Created on: Apr 30, 2019
 *      Author: zhongyun
 */

#ifndef SRC_MAINS_FORCE_HPP_
#define SRC_MAINS_FORCE_HPP_

#include <Eigen/Sparse>
#include <iostream>
#include <mains/FEM/tetrahedron_fem.hpp>
using namespace std;
using namespace Eigen;

class LocalForce{
	public:
		Tetrahedron* ptr_tet;
		Matrix3d getPiolaTensor();
		VectorXd getPiolaVector();
		MatrixXd getDPDF();
		VectorXd getForce();
		MatrixXd getForceJacobian();
		double getInternalPotentialEnergy();
	protected:
	    Matrix3d PiolaTensor;
		VectorXd PiolaVector;
		MatrixXd dPdF;
		VectorXd Force;
		MatrixXd ForceJacobian;
		double  InternalPotentialEnergy=0;


};

double LocalForce::getInternalPotentialEnergy()
{
	return InternalPotentialEnergy;
}
Matrix3d LocalForce::getPiolaTensor()
{
	return PiolaTensor;
}
VectorXd LocalForce::getPiolaVector()
{
	return PiolaVector;
}
MatrixXd LocalForce::getDPDF()
{
	return dPdF;
}
VectorXd LocalForce::getForce()
{
	return Force;
}

MatrixXd LocalForce::getForceJacobian()
{
	return ForceJacobian;
}

class LinearForce: public LocalForce{
	public:
	LinearForce(Tetrahedron* ptr);
	void setInternalPotentialEnergy();
	void setPiolaTensor();
	void setPiolaVector();
	void setDPDF();
	void setForce();
	void setForceJacobian();
	void setAll();
	double miu;
	double lambda;
	double Y=1e10;
	double ni=0.48;
};

LinearForce::LinearForce(Tetrahedron* ptr)
{
	ptr_tet=ptr;
	miu= Y/(2*(1+ni));
	cout<<miu<<"miu"<<endl;
	lambda=Y*ni/((1-2*ni)*(1+ni));
	cout<<lambda<<"lambda"<<endl;
};
void LinearForce::setInternalPotentialEnergy()
{
	Matrix3d F=ptr_tet->getDeformationGradient();
	double vol= ptr_tet->getVolume();
	Matrix3d I;
	I<<1,0,0,
       0,1,0,
	   0,0,1;
	Matrix3d epsilon= (F+F.transpose())/2 - I;
	double phi= miu* epsilon.prod()+
				0.5*lambda*epsilon.trace()*epsilon.trace();
	InternalPotentialEnergy=phi*vol;
}


void LinearForce::setPiolaTensor()
{
	Matrix3d F=ptr_tet->getDeformationGradient();
	double trace_epsilon=F(0,0)+F(1,1)+F(2,2)-3;
	Matrix3d P;

	P<< 2*miu*(F(0,0)-1)+lambda*trace_epsilon,miu*(F(0,1)+F(1,0)),miu*(F(0,2)+F(2,0)),
		miu*(F(0,1)+F(1,0)),2*miu*(F(1,1)-1)+lambda*trace_epsilon,miu*(F(1,2)+F(2,1)),
		miu*(F(0,2)+F(2,0)),miu*(F(1,2)+F(2,1)),2*miu*(F(2,2)-1)+lambda*trace_epsilon;
	PiolaTensor=P;
}

void LinearForce::setPiolaVector()
{
	Matrix3d P;
	P= PiolaTensor;
	Map<VectorXd> vP(P.data(), 9);
	PiolaVector=vP;
}

void LinearForce::setDPDF()
{
	MatrixXd _dPdF(9,9);
	_dPdF<< 2*miu+lambda,  0,  0,  0,      lambda,  0,  0,  0,      lambda,
			           0,miu,  0,miu,           0,  0,  0,  0,           0,
					   0,  0,miu,  0,           0,  0,miu,  0,           0,
			           0,miu,  0,miu,           0,  0,  0,  0,           0,
			      lambda,  0,  0,  0,2*miu+lambda,  0,  0,  0,      lambda,
					   0,  0,  0,  0,           0,miu,  0,miu,           0,
					   0,  0,miu,  0,           0,  0,miu,  0,           0,
					   0,  0,  0,  0,           0,miu,  0,miu,           0,
			      lambda,  0,  0,  0,      lambda,  0,  0,  0,2*miu+lambda;
	dPdF=_dPdF;
}

void LinearForce::setForce()
{
	MatrixXd bB;
	bB =ptr_tet->getBlockB();
	double _V =ptr_tet->getVolume();
	VectorXd _Force;
	_Force= -_V*bB.transpose()*PiolaVector;
	Force=_Force;
}

void LinearForce::setForceJacobian()
{
	MatrixXd bB;
	bB =ptr_tet->getBlockB();
	double _V =ptr_tet->getVolume();
	MatrixXd _ForceJacobian;
	_ForceJacobian= -_V*bB.transpose()*dPdF*bB;
	ForceJacobian=_ForceJacobian;
}

void LinearForce::setAll()
{ setPiolaTensor();
  setPiolaVector();
  setDPDF();
  setForce();
  setForceJacobian();

}


#endif /* SRC_MAINS_FORCE_HPP_ */
