/*
 * piola_tensor.hpp
 *
 *  Created on: Apr 30, 2019
 *      Author: zhongyun
 */

#ifndef SRC_MAINS_FORCE_HPP_
#define SRC_MAINS_FORCE_HPP_

#include <Eigen/Sparse>
#include <iostream>
#include <mains/FEM/tetrahedron_fem.hpp>
using namespace std;
using namespace Eigen;

class LocalForce{
	public:
		Hexahedron* ptr_hex;
		Matrix3d* getPiolaTensor();
		VectorXd* getPiolaVector();
		
		VectorXd getForce();
		MatrixXd getForceJacobian();
		double getInternalPotentialEnergy();
	protected:
	    Matrix3d PiolaTensor[8];
		VectorXd PiolaVector[8];
		
		VectorXd Force;
		MatrixXd ForceJacobian;
		double  InternalPotentialEnergy=0;


};

double LocalForce::getInternalPotentialEnergy()
{
	return InternalPotentialEnergy;
}
Matrix3d* LocalForce::getPiolaTensor()
{
	return PiolaTensor;
}
VectorXd* LocalForce::getPiolaVector()
{
	return PiolaVector;
}

VectorXd LocalForce::getForce()
{
	return Force;
}

MatrixXd LocalForce::getForceJacobian()
{
	return ForceJacobian;
}

class LinearForce: public LocalForce{
	public:
	LinearForce(Hexahedron* ptr);

	void setPiolaTensor();
	void setPiolaVector();
	void setDPDF();
	void setForce();
	void setForceJacobian();
	void setAll();
	double miu=0;
	double lambda=0;
	double Y=1e7;
	double ni=0.48;
	MatrixXd dPdF;
};

LinearForce::LinearForce(Hexahedron* ptr)
{
	ptr_hex=ptr;
	miu= Y/(2*(1+ni));
	lambda=Y*ni/((1-2*ni)*(1+ni));
};


void LinearForce::setPiolaTensor()
{
	Matrix3d* F=ptr_hex->getDeformationGradient();	
	Matrix3d P;
	for (int i=0; i<8; i++)
	{
	double trace_epsilon=F[i](0,0)+F[i](1,1)+F[i](2,2)-3;
	P<< 2*miu*(F[i](0,0)-1)+lambda*trace_epsilon,miu*(F[i](0,1)+F[i](1,0)),miu*(F[i](0,2)+F[i](2,0)),
		miu*(F[i](0,1)+F[i](1,0)),2*miu*(F[i](1,1)-1)+lambda*trace_epsilon,miu*(F[i](1,2)+F[i](2,1)),
		miu*(F[i](0,2)+F[i](2,0)),miu*(F[i](1,2)+F[i](2,1)),2*miu*(F[i](2,2)-1)+lambda*trace_epsilon;
	PiolaTensor[i]=P;
	};
}

void LinearForce::setPiolaVector()
{	for (int i=0; i<8; i++)
	{
	Matrix3d P;
	P= PiolaTensor[i];
	Map<VectorXd> vP(P.data(), 9);
	PiolaVector[i]=vP;
	};
}

void LinearForce::setDPDF()
{
	MatrixXd _dPdF(9,9);
	_dPdF<< 2*miu+lambda,  0,  0,  0,      lambda,  0,  0,  0,      lambda,
			           0,miu,  0,miu,           0,  0,  0,  0,           0,
					   0,  0,miu,  0,           0,  0,miu,  0,           0,
			           0,miu,  0,miu,           0,  0,  0,  0,           0,
			      lambda,  0,  0,  0,2*miu+lambda,  0,  0,  0,      lambda,
					   0,  0,  0,  0,           0,miu,  0,miu,           0,
					   0,  0,miu,  0,           0,  0,miu,  0,           0,
					   0,  0,  0,  0,           0,miu,  0,miu,           0,
			      lambda,  0,  0,  0,      lambda,  0,  0,  0,2*miu+lambda;
	dPdF=_dPdF;
}

void LinearForce::setForce()
{
	MatrixXd* bB;
	bB =ptr_hex->getBlockB();
	double _V =ptr_hex->getVolume();
	VectorXd _Force(24);
	_Force.setZero();
	for (int i=0; i<8; i++)
	{
	_Force+= -_V*bB[i].transpose()*PiolaVector[i];
	};
	Force=0.125*_Force;
}

void LinearForce::setForceJacobian()
{
	MatrixXd* bB;
	bB =ptr_hex->getBlockB();
	double _V =ptr_hex->getVolume();
	MatrixXd _ForceJacobian(24,24);
	_ForceJacobian.setZero();
	for (int i=0; i<8; i++)
	{
	_ForceJacobian+= -_V*bB[i].transpose()*dPdF*bB[i];
	};
	ForceJacobian=0.125*_ForceJacobian;
	
}

void LinearForce::setAll()
{ setPiolaTensor();
cout<<"haha5"<<endl;
  setPiolaVector();
  cout<<"haha6"<<endl;
  setDPDF();
  cout<<"haha7"<<endl;
  setForce();
  cout<<"haha8"<<endl;
  setForceJacobian();

}


#endif /* SRC_MAINS_FORCE_HPP_ */