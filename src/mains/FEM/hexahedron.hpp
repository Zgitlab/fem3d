#ifndef TETRAHEDRON_H_
#define TETRAHEDRON_H_

#include <mains/FEM/node_fem.hpp>
#include <Eigen/Dense>
typedef Matrix<int,8,1> Vector8i;
typedef Matrix<double,3,8> Matrix38d;
typedef Matrix<double,8,3> Matrix83d;

Matrix83d NaturalCoordinate((Matrix83d() << 
                                        -1,-1,-1,
                                        +1,-1,-1,
                                        +1,+1,-1,
                                        -1,+1,-1,
                                        -1,-1,+1,
                                        +1,-1,+1,
                                        +1,+1,+1,
                                        -1,+1,+1).finished());


class Hexahedron {
public:
	 
	void setVolume();
	double getVolume();
	double getMass();
	void preComputeB();
	Hexahedron(){};
	double lambda=-1;
	double miu=-1;//material property

	Node* nodes;
	Vector8i NodesIndex;
	void setDeformationGradient();
	Matrix3d* getDeformationGradient();
	MatrixXd* getBlockB();

private:

    Matrix3d getGeometricJacobian(Matrix83d DNDp);
    Matrix83d getDNDp(Vector3d quadrature_point);
	Matrix3d DeformationGradient[8];
	MatrixXd B[8];//precomputed matrix for deformation gradient
	MatrixXd BlockB[8]; 
	double volume=0; 
	double rho=2700; //density
    
};

Matrix83d Hexahedron::getDNDp(Vector3d quadrature_point)
{
    double Xi = quadrature_point(0);
    double Eta= quadrature_point(1);
    double Miu= quadrature_point(2);
    Matrix83d C= NaturalCoordinate;
    Matrix83d DNDp;
    DNDp << C(0,0)*(1+C(0,1)*Eta)*(1+C(0,2)*Miu), (1+C(0,0)*Xi)*C(0,1)*(1+C(0,2)*Miu), (1+C(0,0)*Xi)*(1+C(0,1)*Eta)*C(0,2),
            C(1,0)*(1+C(1,1)*Eta)*(1+C(1,2)*Miu), (1+C(1,0)*Xi)*C(1,1)*(1+C(1,2)*Miu), (1+C(1,0)*Xi)*(1+C(1,1)*Eta)*C(1,2),
            C(2,0)*(1+C(2,1)*Eta)*(1+C(2,2)*Miu), (1+C(2,0)*Xi)*C(2,1)*(1+C(2,2)*Miu), (1+C(2,0)*Xi)*(1+C(2,1)*Eta)*C(2,2),
            C(3,0)*(1+C(3,1)*Eta)*(1+C(3,2)*Miu), (1+C(3,0)*Xi)*C(3,1)*(1+C(3,2)*Miu), (1+C(3,0)*Xi)*(1+C(3,1)*Eta)*C(3,2),
            C(4,0)*(1+C(4,1)*Eta)*(1+C(4,2)*Miu), (1+C(4,0)*Xi)*C(4,1)*(1+C(4,2)*Miu), (1+C(4,0)*Xi)*(1+C(4,1)*Eta)*C(4,2),
            C(5,0)*(1+C(5,1)*Eta)*(1+C(5,2)*Miu), (1+C(5,0)*Xi)*C(5,1)*(1+C(5,2)*Miu), (1+C(5,0)*Xi)*(1+C(5,1)*Eta)*C(5,2),
            C(6,0)*(1+C(6,1)*Eta)*(1+C(6,2)*Miu), (1+C(6,0)*Xi)*C(6,1)*(1+C(6,2)*Miu), (1+C(6,0)*Xi)*(1+C(6,1)*Eta)*C(6,2),
            C(7,0)*(1+C(7,1)*Eta)*(1+C(7,2)*Miu), (1+C(7,0)*Xi)*C(7,1)*(1+C(7,2)*Miu), (1+C(7,0)*Xi)*(1+C(7,1)*Eta)*C(7,2);

	
    return 0.125*DNDp;

}

Matrix3d Hexahedron::getGeometricJacobian(Matrix83d DNDp)
{
	 MatrixXd N0(3,8);
	 N0<<  nodes[NodesIndex(0)].getInitialPosition(),
	 	   nodes[NodesIndex(1)].getInitialPosition(),
		   nodes[NodesIndex(2)].getInitialPosition(),
		   nodes[NodesIndex(3)].getInitialPosition(),
	 	   nodes[NodesIndex(4)].getInitialPosition(),
		   nodes[NodesIndex(5)].getInitialPosition(),
	 	   nodes[NodesIndex(6)].getInitialPosition(),
		   nodes[NodesIndex(7)].getInitialPosition();
    
	Matrix3d Jacobian;
	Jacobian=N0*DNDp;
	
	return Jacobian;
}
void Hexahedron::preComputeB()
{
	Matrix83d DNDp;
    
	Vector3d quadrature_point;
	double alpha=1.0/sqrt(3.0);
	
	for (int i=0; i<8;i++)
	{	quadrature_point=alpha*(NaturalCoordinate.row(i));
		DNDp= getDNDp(quadrature_point);
		Matrix3d JX;
		JX= getGeometricJacobian(DNDp);
		B[i]=DNDp*JX.inverse();
		BlockB[i].resize(9,24);
		BlockB[i]     <<B[i](0,0),0,0,B[i](1,0),0,0,B[i](2,0),0,0,B[i](3,0),0,0,B[i](4,0),0,0,B[i](5,0),0,0,B[i](6,0),0,0,B[i](7,0),0,0,
						0,B[i](0,0),0,0,B[i](1,0),0,0,B[i](2,0),0,0,B[i](3,0),0,0,B[i](4,0),0,0,B[i](5,0),0,0,B[i](6,0),0,0,B[i](7,0),0,
						0,0,B[i](0,0),0,0,B[i](1,0),0,0,B[i](2,0),0,0,B[i](3,0),0,0,B[i](4,0),0,0,B[i](5,0),0,0,B[i](6,0),0,0,B[i](7,0),
						B[i](0,1),0,0,B[i](1,1),0,0,B[i](2,1),0,0,B[i](3,1),0,0,B[i](4,1),0,0,B[i](5,1),0,0,B[i](6,1),0,0,B[i](7,1),0,0,
						0,B[i](0,1),0,0,B[i](1,1),0,0,B[i](2,1),0,0,B[i](3,1),0,0,B[i](4,1),0,0,B[i](5,1),0,0,B[i](6,1),0,0,B[i](7,1),0,
						0,0,B[i](0,1),0,0,B[i](1,1),0,0,B[i](2,1),0,0,B[i](3,1),0,0,B[i](4,1),0,0,B[i](5,1),0,0,B[i](6,1),0,0,B[i](7,1),
						B[i](0,2),0,0,B[i](1,2),0,0,B[i](2,2),0,0,B[i](3,2),0,0,B[i](4,2),0,0,B[i](5,2),0,0,B[i](6,2),0,0,B[i](7,2),0,0,
						0,B[i](0,2),0,0,B[i](1,2),0,0,B[i](2,2),0,0,B[i](3,2),0,0,B[i](4,2),0,0,B[i](5,2),0,0,B[i](6,2),0,0,B[i](7,2),0,
						0,0,B[i](0,2),0,0,B[i](1,2),0,0,B[i](2,2),0,0,B[i](3,2),0,0,B[i](4,2),0,0,B[i](5,2),0,0,B[i](6,2),0,0,B[i](7,2);
	};
	
	

}

void Hexahedron::setDeformationGradient()
{
	MatrixXd Nx(3,8);
	Nx<<nodes[NodesIndex(0)].getPosition(),
		nodes[NodesIndex(1)].getPosition(),
		nodes[NodesIndex(2)].getPosition(),
		nodes[NodesIndex(3)].getPosition(),
		nodes[NodesIndex(4)].getPosition(),
		nodes[NodesIndex(5)].getPosition(),
		nodes[NodesIndex(6)].getPosition(),
		nodes[NodesIndex(7)].getPosition();
	for(int i=0;i<8;i++)
	{
		DeformationGradient[i]=Nx*B[i];
	};
    

}

Matrix3d* Hexahedron::getDeformationGradient()
{
	return DeformationGradient;
}

MatrixXd* Hexahedron::getBlockB()
{
	return BlockB;
}

void Hexahedron::setVolume()
{	
	Vector3d x17,x75,x74,x72,x56,x58,x48,x43,x23,x26;
	x17= nodes[NodesIndex(6)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition();
	x75= nodes[NodesIndex(4)].getInitialPosition()- nodes[NodesIndex(6)].getInitialPosition();
	x74= nodes[NodesIndex(3)].getInitialPosition()- nodes[NodesIndex(6)].getInitialPosition();
	x72= nodes[NodesIndex(1)].getInitialPosition()- nodes[NodesIndex(6)].getInitialPosition();
	x56= nodes[NodesIndex(5)].getInitialPosition()- nodes[NodesIndex(4)].getInitialPosition();
	x58= nodes[NodesIndex(7)].getInitialPosition()- nodes[NodesIndex(4)].getInitialPosition();
	x48= nodes[NodesIndex(7)].getInitialPosition()- nodes[NodesIndex(3)].getInitialPosition();
	x43= nodes[NodesIndex(2)].getInitialPosition()- nodes[NodesIndex(3)].getInitialPosition();
	x23= nodes[NodesIndex(2)].getInitialPosition()- nodes[NodesIndex(1)].getInitialPosition();
	x26= nodes[NodesIndex(5)].getInitialPosition()- nodes[NodesIndex(1)].getInitialPosition();
	Matrix3d _T[6];
	double _volume=0;
	
	_T[0]<< x17,x75,x56;
	_T[1]<< x17,x75,x58;
	_T[2]<< x17,x74,x48;
	_T[3]<< x17,x74,x43;
	_T[4]<< x17,x72,x23;
	_T[5]<< x17,x72,x26;
	
	for(int i=0;i<6;i++)
	{
	_volume+=abs(_T[i].determinant()/6);
	};
	
	volume=_volume;
}

double Hexahedron::getVolume()
{
	return volume;
	}
double  Hexahedron::getMass()
{
	return rho*volume;
}

#endif /* TETRAHEDRON_H_ */
