
#pragma once
#include <mains/FEM/system_fem.hpp>


class IntegratorFEM {
public:
	void doStepImplicit();
	void doStepImplicit(Vector3d Gravity);
	void doStepImplicit(Vector3d gravity, std::vector<bool> v_isFixed);

	systemFEM* pS;
	int *fixed_points;  //todo
	int n_fixed_points=0; //todo

private:
	
	double dt=1e-3;

};

void IntegratorFEM::doStepImplicit()
{
	VectorXd V0=pS->getVelocities();
	VectorXd X0=pS->getPositions();

	SpMat M= pS->getMass();
	pS->setForces();
	VectorXd F=pS->getForces();
	SpMat dfdx=pS->getForceJacobian();

	SpMat I(3*pS->n_nodes,3*pS->n_nodes);
	I.setIdentity();

	SpMat lhs;

	lhs=M-dt*dt*dfdx;

	VectorXd rhs;


	rhs=dt*(F+ dt* dfdx* V0);


	VectorXd V1;
	Eigen::SimplicialCholesky<SpMat> chol(lhs);
	Eigen::VectorXd dV = chol.solve(rhs);
	Vector3d dv0;
	dv0<<0,0,0;

	//for(int i=0; i<n_fixed_points; i++)
	//{
	//	dV.segment(3*fixed_points[i],3)=dv0;
	//};

	V1= V0+dV;
	VectorXd X1;
	X1=X0+ dt* V1;

	pS->setPositions(X1);
	pS->setVelocities(V1);
	pS->update();

};

void IntegratorFEM::doStepImplicit(Vector3d gravity)
{	

	VectorXd V0=pS->getVelocities();
	VectorXd X0=pS->getPositions();

	SpMat M= pS->getMass();
	pS->setForces();
	VectorXd F=pS->getForces();
	
	SpMat dfdx=pS->getForceJacobian();

	int nn= pS->n_nodes;
	SpMat I(3*nn,3*nn);
	I.setIdentity();
	VectorXd g(3*nn);

	for(int i=0;i<nn;i++)
	{	
		g.segment(3*i,3)=gravity;

	};
	

	SpMat lhs;

	lhs=M-dt*dt*dfdx;

	VectorXd rhs;


	rhs=dt*(F- M*g + dt* dfdx* V0);


	VectorXd V1;
	Eigen::SimplicialCholesky<SpMat> chol(lhs);
	Eigen::VectorXd dV = chol.solve(rhs);
	Vector3d dv0;
	dv0<<0,0,0;

	V1= V0+dV;
	VectorXd X1;
	X1=X0+ dt* V1;

	pS->setPositions(X1);
	pS->setVelocities(V1);
	pS->update();

};

void IntegratorFEM::doStepImplicit(Vector3d gravity, std::vector<bool> v_isFixed)
{	

	VectorXd V0=pS->getVelocities();
	VectorXd X0=pS->getPositions();

	SpMat M= pS->getMass();
	pS->setForces();
	VectorXd F=pS->getForces();
	SpMat dfdx=pS->getForceJacobian();
	int nn= pS->n_nodes;
	SpMat I(3*nn,3*nn);
	I.setIdentity();
	VectorXd g(3*nn);

	for(int i=0;i<nn;i++)
	{	
		g.segment(3*i,3)=gravity;

	};
	

	SpMat lhs;

	lhs=M-dt*dt*dfdx;

	VectorXd rhs;


	rhs=dt*(F+ M*g + dt* dfdx* V0);

	for (int k = 0; k < lhs.outerSize(); ++k)
	{
		for (SpMat::InnerIterator it(lhs, k); it; ++it)
			{
				if (v_isFixed[it.row()] || v_isFixed[it.col()])
				{
					if (it.row() == it.col())
						 it.valueRef() = 1.0;
					else it.valueRef() = 0.0;
				};
			};
				
	};

	for (int k = 0; k < rhs.rows(); ++k)
	{
		if (v_isFixed[k])
				{
					rhs(k)=0;
				};
	};




	VectorXd V1;
	Eigen::SimplicialCholesky<SpMat> chol(lhs);
	Eigen::VectorXd dV = chol.solve(rhs);

	V1= V0+dV;
	VectorXd X1;
	X1=X0+ dt* V1;
	pS->setPositions(X1);
	pS->setVelocities(V1);
	pS->update();

};