#ifndef BASICELEMENT_H_
#define BASICELEMENT_H_

#include <mains/FEM/node_fem.hpp>
class BasicElement {
public:
	void preComputeB();//precomputing necessary matrix for deformation gradient
	void setDeformationGradient();
	Matrix3d getDeformationGradient();
	MatrixXd getBlockB();
	
	void setVolume();
	double getVolume();
	double getMass();

	Node* nodes;
	VectorXi NodesIndex;


private:
	Matrix3d DeformationGradient;
	MatrixXd B;//precomputed matrix for deformation gradient
	MatrixXd BlockB;
	double volume=0;
	double rho=1; //density
};


double BasicElement::getVolume()
{
	return volume;
	}
double BasicElement::getMass()
{
	return rho*volume;
	}
MatrixXd BasicElement::getBlockB()
{
	return BlockB;
}

#endif /* TETRAHEDRON_H_ */
