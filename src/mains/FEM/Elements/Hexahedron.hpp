#ifndef TETRAHEDRON_H_
#define TETRAHEDRON_H_

#include <mains/FEM/node_fem.hpp>
#include <Eigen/Dense>
typedef Matrix<int,8,1> Vector8i;
typedef Matrix<double,3,8> Matrix38d;
typedef Matrix<double,8,3> Matrix83d;

Matrix83d NaturalCoordinate((Matrix83d() << 
                                        -1,-1,-1,
                                        +1,-1,-1,
                                        +1,+1,-1,
                                        -1,+1,-1,
                                        -1,-1,+1,
                                        +1,-1,+1,
                                        +1,+1,+1,
                                        -1,+1,+1).finished());


class Hexahedron {
public:
	 
	void setVolume();
	double getVolume();
	double getMass();
	void preComputeB();

	Node* nodes;
	Vector8i NodesIndex;
	void setDeformationGradient();
	Matrix3d getDeformationGradient();


private:

    Matrix3d getGeometricJacobian(Matrix83d DNDp);
    Matrix83d getDNDp(Vector3d quadrature_point);
	Matrix3d DeformationGradient;
	MatrixXd B;//precomputed matrix for de formation gradient
	MatrixXd BlockB; 
	double volume=0; 
	double rho=1; //density
    
};

Matrix83d Hexahedron::getDNDp(Vector3d quadrature_point)
{
    double Xi = quadrature_point(0);
    double Eta= quadrature_point(1);
    double Miu= quadrature_point(2);
    Matrix83d C= NaturalCoordinate;
    Matrix83d DNDp;
    DNDp << C(0,0)*(1+C(0,1)*Eta)*(1+C(0,2)*Miu), (1+C(0,0)*Xi)*C(0,1)*(1+C(0,2)*Miu), (1+C(0,0)*Xi)*(1+C(0,1)*Eta)*C(0,2),
            C(1,0)*(1+C(1,1)*Eta)*(1+C(1,2)*Miu), (1+C(1,0)*Xi)*C(1,1)*(1+C(1,2)*Miu), (1+C(1,0)*Xi)*(1+C(1,1)*Eta)*C(1,2),
            C(2,0)*(1+C(2,1)*Eta)*(1+C(2,2)*Miu), (1+C(2,0)*Xi)*C(2,1)*(1+C(2,2)*Miu), (1+C(2,0)*Xi)*(1+C(2,1)*Eta)*C(2,2),
            C(3,0)*(1+C(3,1)*Eta)*(1+C(3,2)*Miu), (1+C(3,0)*Xi)*C(3,1)*(1+C(3,2)*Miu), (1+C(3,0)*Xi)*(1+C(3,1)*Eta)*C(3,2),
            C(4,0)*(1+C(4,1)*Eta)*(1+C(4,2)*Miu), (1+C(4,0)*Xi)*C(4,1)*(1+C(4,2)*Miu), (1+C(4,0)*Xi)*(1+C(4,1)*Eta)*C(4,2),
            C(5,0)*(1+C(5,1)*Eta)*(1+C(5,2)*Miu), (1+C(5,0)*Xi)*C(5,1)*(1+C(5,2)*Miu), (1+C(5,0)*Xi)*(1+C(5,1)*Eta)*C(5,2),
            C(6,0)*(1+C(6,1)*Eta)*(1+C(6,2)*Miu), (1+C(6,0)*Xi)*C(6,1)*(1+C(6,2)*Miu), (1+C(6,0)*Xi)*(1+C(6,1)*Eta)*C(6,2),
            C(7,0)*(1+C(7,1)*Eta)*(1+C(7,2)*Miu), (1+C(7,0)*Xi)*C(7,1)*(1+C(7,2)*Miu), (1+C(7,0)*Xi)*(1+C(7,1)*Eta)*C(7,2);

	
    return 0.125*DNDp;

}

Matrix3d Hexahedron::getGeometricJacobian(Matrix83d DNDp)
{
	 MatrixXd N0(3,8);
	 N0<<  nodes[NodesIndex(0)].getInitialPosition(),
	 	   nodes[NodesIndex(1)].getInitialPosition(),
		   nodes[NodesIndex(2)].getInitialPosition(),
		   nodes[NodesIndex(3)].getInitialPosition(),
	 	   nodes[NodesIndex(4)].getInitialPosition(),
		   nodes[NodesIndex(5)].getInitialPosition(),
	 	   nodes[NodesIndex(6)].getInitialPosition(),
		   nodes[NodesIndex(7)].getInitialPosition();
    
	Matrix3d Jacobian;
	Jacobian=N0*DNDp;
	std::cout<<Jacobian<<"Jacobian"<<std::endl;
	return Jacobian;
}
void Hexahedron::preComputeB()
{
	Matrix83d DNDp;
    Vector3d quadrature_point;
	double alpha=1.0/sqrt(3.0);
	Matrix83d _B;
	_B.setZero();
	Matrix83d C;
	
	for (int i=0; i<8;i++)
	{	quadrature_point=alpha*(NaturalCoordinate.row(i));
		DNDp= getDNDp(quadrature_point);
		Matrix3d JX;
		JX= getGeometricJacobian(DNDp);
		_B+=DNDp*JX.inverse();
	};
	
	B=_B/8.0;

	
	MatrixXd _blockB(9,24);
	_blockB<<	B(0,0),0,0,B(1,0),0,0,B(2,0),0,0,B(3,0),0,0,B(4,0),0,0,B(5,0),0,0,B(6,0),0,0,B(7,0),0,0,
				0,B(0,0),0,0,B(1,0),0,0,B(2,0),0,0,B(3,0),0,0,B(4,0),0,0,B(5,0),0,0,B(6,0),0,0,B(7,0),0,
				0,0,B(0,0),0,0,B(1,0),0,0,B(2,0),0,0,B(3,0),0,0,B(4,0),0,0,B(5,0),0,0,B(6,0),0,0,B(7,0),
				B(0,1),0,0,B(1,1),0,0,B(2,1),0,0,B(3,1),0,0,B(4,1),0,0,B(5,1),0,0,B(6,1),0,0,B(7,1),0,0,
				0,B(0,1),0,0,B(1,1),0,0,B(2,1),0,0,B(3,1),0,0,B(4,1),0,0,B(5,1),0,0,B(6,1),0,0,B(7,1),0,
				0,0,B(0,1),0,0,B(1,1),0,0,B(2,1),0,0,B(3,1),0,0,B(4,1),0,0,B(5,1),0,0,B(6,1),0,0,B(7,1),
				B(0,2),0,0,B(1,2),0,0,B(2,2),0,0,B(3,2),0,0,B(4,2),0,0,B(5,2),0,0,B(6,2),0,0,B(7,2),0,0,
				0,B(0,2),0,0,B(1,2),0,0,B(2,2),0,0,B(3,2),0,0,B(4,2),0,0,B(5,2),0,0,B(6,2),0,0,B(7,2),0,
				0,0,B(0,2),0,0,B(1,2),0,0,B(2,2),0,0,B(3,2),0,0,B(4,2),0,0,B(5,2),0,0,B(6,2),0,0,B(7,2);

	BlockB=_blockB;
	std::cout<<B<<"B"<<std::endl;

}

void Hexahedron::setDeformationGradient()
{
	MatrixXd Nx(3,8);
	Nx<<nodes[NodesIndex(0)].getPosition(),
		nodes[NodesIndex(1)].getPosition(),
		nodes[NodesIndex(2)].getPosition(),
		nodes[NodesIndex(3)].getPosition(),
		nodes[NodesIndex(4)].getPosition(),
		nodes[NodesIndex(5)].getPosition(),
		nodes[NodesIndex(6)].getPosition(),
		nodes[NodesIndex(7)].getPosition();
	std::cout<<Nx<<"Nx"<<std::endl;
	std::cout<<B<<"000Nx"<<std::endl;
	DeformationGradient=Nx*B;
    std::cout<<DeformationGradient<<"DeformationGradient"<<std::endl;

}

Matrix3d Hexahedron::getDeformationGradient()
{
	return DeformationGradient;
}

/*
void Tetrahedron::setVolume()
{
	MatrixXd _T(3,3);
	MatrixXd _Tnew(3,3);
	double _volume;
	_T<< nodes[NodesIndex(1)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
		 nodes[NodesIndex(2)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
		 nodes[NodesIndex(3)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition();

	_volume=(_T.determinant()/6);

	if (_volume<0)
		{
		int _n0=NodesIndex(0);
		int _n3=NodesIndex(3);
		NodesIndex(0)=_n3;
		NodesIndex(3)=_n0;

		_Tnew<<  nodes[NodesIndex(1)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
				 nodes[NodesIndex(2)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition(),
				 nodes[NodesIndex(3)].getInitialPosition()- nodes[NodesIndex(0)].getInitialPosition();

		_volume=(_Tnew.determinant()/6);
		};

	volume=_volume;
}
double Tetrahedron::getVolume()
{
	return volume;
	}
double Tetrahedron::getMass()
{
	return rho*volume;
	}
MatrixXd Tetrahedron::getBlockB()
{
	return BlockB;
}
*/
#endif /* TETRAHEDRON_H_ */
