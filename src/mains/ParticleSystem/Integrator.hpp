

#include <mains/ParticleSystem/particleSystem.hpp>


class Integrator {
public:

	void doStepExplicit();
	void doStepImplicit();
	particleSystem* pS;

private:

	double dt=1e-100;
	int *fixed_points;  //todo
	int n_fixed_points=0; //todo
};

void Integrator::doStepExplicit()
{
	VectorXd V0=pS->getVelocities();
	VectorXd X0=pS->getPositions();
	SpMat MInverse= pS->getMassInverse();

	VectorXd F=pS->getForces();

	VectorXd V1;
	V1=V0+ dt* MInverse *F;

	VectorXd X1;
	X1=X0+ dt* V1;



	pS->setPositions(X1);
	pS->setVelocities(V1);
	pS->update();

};

void Integrator::doStepImplicit()
{
	VectorXd V0=pS->getVelocities();
	VectorXd X0=pS->getPositions();

	SpMat MInverse= pS->getMassInverse();
	pS->setForceSpatialDerivatives();
	pS->setVelocityDerivatives();
	SpMat dfdx=pS->getForceSpatialDerivatives();
	SpMat dfdv=pS->getVelocityDerivatives();

	SpMat I(3*pS->n_particles,3*pS->n_particles);
	I.setIdentity();

	SpMat lhs;

	lhs=I-dt*dt* MInverse*dfdx-dt*MInverse*dfdv;

	VectorXd rhs;
	VectorXd F=pS->getForces();

	rhs=dt*MInverse*(F+ dt* dfdx* V0);


	VectorXd V1;
	Eigen::SimplicialCholesky<SpMat> chol(lhs);
	Eigen::VectorXd dV = chol.solve(rhs);

	V1= V0+dV;
	VectorXd X1;
	X1=X0+ dt* V1;

	pS->setPositions(X1);
	pS->setVelocities(V1);
	pS->update();

};
