

#include <Eigen/Sparse>
#include <iostream>
using namespace std;
using namespace Eigen;



class Particle {
public:
	

	Vector3d getPosition();
	void setPosition(Vector3d pos);

	Vector3d getVelocity();
	void setVelocity(Vector3d Vel);

	double getMass();
	void setMass(double m);

private:
	Vector3d position;
	Vector3d velocity;
	double mass;
};

Vector3d Particle::getPosition()
{
	return position;
};

void Particle::setPosition(Vector3d pos)
{
	position= pos;
};

Vector3d Particle::getVelocity()
{
	return velocity;
};


void Particle::setVelocity(Vector3d vel)
{
	velocity=vel;
};

double Particle::getMass(){
	return mass;
};

void Particle::setMass(double m){
	mass=m;
};
