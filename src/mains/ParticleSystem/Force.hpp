#include <iostream>
using namespace std;

class Force {

public:
	

	void setParticles(Particle a[], int i, int j);
	void setForce();

	void set_dfdx();
	void set_dfdv();

	VectorXd getForce();
	Matrix3d get_dfdx();
	Matrix3d get_dfdv();

	int Obj1, Obj2;


private:

	Vector3d force;

	Particle *P1, *P2;
	double L0=0.5;
	double stiffness=1;
	double damping=10;
	Matrix3d dfdx;
	Matrix3d dfdv;
};

void Force::setParticles(Particle p[], int i, int j)
{
	P1=&p[i];
	P2=&p[j];
	Obj1=i;
	Obj2=j;
}

void Force::setForce()
{ Vector3d _position,_velocity;
  _position=P1->getPosition()-P2->getPosition();
  _velocity=P1->getVelocity()-P2->getVelocity();
  double distance= _position.norm();
  force= -stiffness*(1-L0/distance)*_position
		 -damping*_velocity;
};

void Force::set_dfdx()
{
	Vector3d _position;
	_position=P1->getPosition()-P2->getPosition();
	double distance= _position.norm();

	MatrixXd I =  Matrix<double, 3, 3>::Identity();
	Vector3d _n= _position/distance;
	dfdx=stiffness*((L0/distance-1)*I - L0/distance*(_n*_n.transpose()));

};


Matrix3d Force::get_dfdx()
{
	return dfdx;
};

void Force::set_dfdv()
{
	Vector3d _position;
	_position=P1->getPosition()-P2->getPosition();
	double distance= _position.norm();

	MatrixXd I =  Matrix<double, 3, 3>::Identity();
	Vector3d _n= _position/distance;
	dfdv=-damping*(_n*_n.transpose());

};

Matrix3d Force::get_dfdv()
{
	return dfdv;
};

VectorXd Force::getForce()
{
	return force;
}

