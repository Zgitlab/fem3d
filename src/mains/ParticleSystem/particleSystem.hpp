#include <mains/ParticleSystem/Particle.hpp>
#include <mains/Force.hpp>
typedef Eigen::SparseMatrix<double> SpMat; 
typedef Eigen::Triplet<double> T;

class particleSystem {
public:
	int n_particles;
	int n_interactions;

	particleSystem(int np,int ni, Particle* ptr_particles,Force* ptr_interactions);
	Particle *Particles;
	Force *Interactions;

	VectorXd getPositions();
	void setPositions(VectorXd pos);
	void setPositions();

	VectorXd getVelocities();
	void setVelocities(VectorXd vel);
	void setVelocities();

	SpMat getMass();
	SpMat getMassInverse();
	void setMass();

	VectorXd getForces();
	void setForces();

	SpMat getForceSpatialDerivatives();
	void setForceSpatialDerivatives();

	SpMat getVelocityDerivatives();
	void setVelocityDerivatives();


	void update();



private:
	VectorXd Forces;
	SpMat Mass;
	SpMat dFdV;
	SpMat dFdX;
	SpMat MassInverse;
	VectorXd Positions;
	VectorXd Velocities;




};

particleSystem::particleSystem(int np, int ni, Particle* ptr_particles, Force* ptr_interactions)
{	Particles = ptr_particles;
	Interactions=ptr_interactions;
	n_particles=np;
	n_interactions=ni;
	Forces=VectorXd::Zero(3*n_particles);
	Positions=VectorXd::Zero(3*n_particles);
	Velocities=VectorXd::Zero(3*n_particles);

};


VectorXd particleSystem::getPositions() {
	return Positions;
};

void particleSystem::setPositions(VectorXd pos) {
	 Positions=pos;
};

void particleSystem::setPositions() {

	 Positions=VectorXd::Zero(3*n_particles);
     for(int i=0; i<n_particles; i++)
     {
    	 Positions.segment(3*i,3)=Particles[i].getPosition();

     };

};

VectorXd particleSystem::getVelocities() {
	return Velocities;
};

void particleSystem::setVelocities(VectorXd vol) {
	 Velocities=vol;
};

void particleSystem::setVelocities() {
	 Velocities=VectorXd::Zero(3*n_particles);
	     for(int i=0; i<n_particles; i++)
	     {
	    	 Velocities.segment(3*i,3)=Particles[i].getVelocity();
	     };
};


SpMat particleSystem::getMass()
{
	return Mass;
}

SpMat particleSystem::getMassInverse()
{
	return MassInverse;
}

void particleSystem::setMass()
{   SpMat mass(3*n_particles,3*n_particles);
	SpMat massInverse(3*n_particles,3*n_particles);
	std::vector<T> tripletList;
	std::vector<T> tripletListInverse;
	tripletList.reserve(3*n_particles);

    for(int i=0; i<n_particles; i++)
    {
    	double m=Particles[i].getMass();

    	tripletList.push_back(T(3*i,3*i,m));
    	tripletList.push_back(T(3*i+1,3*i+1,m));
    	tripletList.push_back(T(3*i+2,3*i+2,m));

    	tripletListInverse.push_back(T(3*i,3*i,1/m));
    	tripletListInverse.push_back(T(3*i+1,3*i+1,1/m));
    	tripletListInverse.push_back(T(3*i+2,3*i+2,1/m));
    };

    mass.setFromTriplets(tripletList.begin(), tripletList.end());
    massInverse.setFromTriplets(tripletListInverse.begin(), tripletListInverse.end());
    Mass=mass;
    MassInverse=massInverse;

}

VectorXd particleSystem::getForces()
{	setForces();
	return Forces;
}

void particleSystem::setForces()
{
	Forces=VectorXd::Zero(3*n_particles);
	for (int i=0; i<n_interactions; i++)
	{
		Interactions[i].setForce();

		int ind1= Interactions[i].Obj1;
		int ind2= Interactions[i].Obj2;

		Forces.segment(3*ind1,3) += Interactions[i].getForce();
		Forces.segment(3*ind2,3) -= Interactions[i].getForce();
	};

	VectorXd FieldForce,acr;
	Vector3d acceleration;
	acceleration<<0,0,0;
	acr=acceleration.replicate(n_particles,1);
	FieldForce=getMass()*acr;
	Forces+=FieldForce;
}

void particleSystem::setForceSpatialDerivatives()
{
	SpMat _dFdX(3*n_particles,3*n_particles);
	_dFdX.setZero();

	std::vector<T> tripletList;
	tripletList.reserve(9*n_particles*n_particles);

	MatrixXd _Diagonal(3*n_particles,3);
	_Diagonal.setZero();

	for (int k=0; k<n_interactions; k++)
		{
			Interactions[k].set_dfdx();
			Matrix3d m=Interactions[k].get_dfdx();

			int IND1=Interactions[k].Obj1;
			int IND2=Interactions[k].Obj2;

			_Diagonal.block<3,3>(3*IND1,0) += m;
			_Diagonal.block<3,3>(3*IND2,0) += m;

			for (int i=0; i<3; i++)
				{for (int j=0; j<3; j++)
			 	 	 {
						if (m(i,j)!=0)
			 	 		 	 {
							  tripletList.push_back(T(3*IND1+i,3*IND2+j,-m(i,j)));
			 	 		 	  tripletList.push_back(T(3*IND2+i,3*IND1+j,-m(i,j)));
			 	 		 	  };
			 	 	 };
				};

		};
		for (int i=0; i<3*n_particles; i++)
			{for (int j=0; j<3; j++)
				 {
					if (_Diagonal(i,j)!=0)
				 	 	{
						tripletList.push_back(T(i, 3*(i/3)+j,_Diagonal(i,j)));
				 	 	};
				 };
			};

		_dFdX.setFromTriplets(tripletList.begin(), tripletList.end());
		dFdX=_dFdX;
}

void particleSystem::setVelocityDerivatives()
{
	SpMat _dFdV(3*n_particles,3*n_particles);
	_dFdV.setZero();

	std::vector<T> tripletList;
	tripletList.reserve(9*n_particles*n_particles);

    MatrixXd _Diagonal(3*n_particles,3);
    _Diagonal.setZero();

	for (int k=0; k<n_interactions; k++)
	{
		Interactions[k].set_dfdv();
		Matrix3d m=Interactions[k].get_dfdv();

		int IND1=Interactions[k].Obj1;
		int IND2=Interactions[k].Obj2;

		_Diagonal.block<3,3>(3*IND1,0) += m;
		_Diagonal.block<3,3>(3*IND2,0) += m;

		for (int i=0; i<3; i++)
			{for (int j=0; j<3; j++)
		 	 	 {
					if (m(i,j)!=0)
		 	 		 	 {
						  tripletList.push_back(T(3*IND1+i,3*IND2+j,-m(i,j)));
		 	 		 	  tripletList.push_back(T(3*IND2+i,3*IND1+j,-m(i,j)));
		 	 		 	  };
		 	 	 };
			};

	};
	for (int i=0; i<3*n_particles; i++)
		{for (int j=0; j<3; j++)
			 {
				if (_Diagonal(i,j)!=0)
			 	 	{
					tripletList.push_back(T(i, 3*(i/3)+j,_Diagonal(i,j)));
			 	 	};
			 };
		};

	_dFdV.setFromTriplets(tripletList.begin(), tripletList.end());
	dFdV=_dFdV;
}

SpMat particleSystem::getForceSpatialDerivatives()
{
	return dFdX;
};


SpMat particleSystem::getVelocityDerivatives()
{
	return dFdV;
};

void particleSystem::update()
{
	 for(int i=0; i<n_particles; i++)
	     {
	    	 Particles[i].setPosition(Positions.segment(3*i,3));
	    	 Particles[i].setVelocity(Velocities.segment(3*i,3));
	     };
	 for(int i=0; i<n_interactions; i++)
	 	{
		 	Interactions[i].setForce();
	 	};
}


