
using namespace std;

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>
#include <mains/FEM/hexahedron.hpp>
#include <mains/FEM/force_hex.hpp>
#include <mains/FEM/system_hex.hpp>
#include <mains/FEM/Integrator_fem.hpp>
#include <utils/hexahedronMesh.hpp>
#include <vector>
namespace Mains
{
void meshtest()
{   
    Window window("Visualizer");
	window.setSize(1200, 700);
    HexahedronCuboidMesh a(20,5, 5, 0.05);
    //HexahedronCuboidMesh a(2,2, 2, 0.05);
    Node nodes[a.N_nodes];

    cout<<a.getInnerProductOfDerivative()<<endl;

    for(int i=0; i<a.N_nodes;i++)
        {   
            Vector3d pos,vel;
            vel<< 0,0,0;
            pos=a.getNodesPositions().segment(3*i,3);
            nodes[i].setInitialPosition(pos);
            nodes[i].setPosition(pos);
            nodes[i].setVelocity(vel);
        };
    
   
    Hexahedron hex[a.N_hex];
    MatrixXi nodesIndex= a.getHexahedrons();
    
    for(int i=0; i<a.N_hex; i++)
    {
        hex[i].nodes=nodes;
        hex[i].NodesIndex=nodesIndex.row(i);
        hex[i].preComputeB();
        hex[i].setDeformationGradient();
        hex[i].setVolume();
    }
    /*
    for (int j=1; j<4; j++)
    {
        for(int i=(a.ny*j-1)*(a.ny-1)*(a.nz-1); i<(a.ny*j+1)*(a.ny-1)*(a.nz-1); i++)
        {   
            double Y=8e5;
	        double ni=0.48;
            hex[i].miu= Y/(2*(1+ni));
	        hex[i].lambda=Y*ni/((1-2*ni)*(1+ni));
        }
    }
    */

    MatrixXd A= a.getNodesPositions();
    A.resize(3,a.N_nodes);
    MatrixXd V1=A.transpose();
   
    MeshTri3D mesh;

	mesh.updatePointBuffer(V1);
   
	mesh.addEdgeLayer("Edges")->updateEdgeBuffer(a.getEdges());

    systemFEM sysHex(hex,nodes,a.N_hex,a.N_nodes);

    sysHex.initialize();
   
    IntegratorFEM Integ;
    Integ.pS= &sysHex;
    Vector3d gravity;
    gravity<<0,-10,0;


    vector<bool> v_isFixed;
    for(int i=0;i<3*a.N_nodes;i++)
    {
        if (i<3*a.ny*a.nz)
            v_isFixed.push_back(true);
        else 
            v_isFixed.push_back(false);    

    }

    while(!window.shouldClose())
    {   
        Integ.doStepImplicit(gravity,v_isFixed);
     
    	A= sysHex.getPositions();
    	A.resize(3,a.N_nodes);
    	V1=A.transpose();
        

        mesh.updatePointBuffer(V1);
        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    };

}
void hexatest()
{   
    Window window("Visualizer");
	window.setSize(1200, 700);

    Node nodes[8];
    Vector8i ind;
    ind <<0,1,2,3,4,5,6,7;
    Vector3d pos;
    Vector3d vel;
    vel<< 0,0,0;
    pos<< -1,-1,-1;
    nodes[0].setInitialPosition(pos);
    nodes[0].setPosition(0.7*pos);
    nodes[0].setVelocity(vel);
    pos<<+1,-1,-1;
    nodes[1].setInitialPosition(pos);
    nodes[1].setPosition(0.7*pos);
    nodes[1].setVelocity(vel);
    pos<< 1, 1,-1;
    nodes[2].setInitialPosition(pos);
    nodes[2].setPosition(0.7*pos);
    nodes[2].setVelocity(vel);
    pos<< -1, 1,-1;
    nodes[3].setInitialPosition(pos);
    nodes[3].setPosition(0.7*pos);
    nodes[3].setVelocity(vel);
    pos<< -1, -1, 1;
    nodes[4].setInitialPosition(pos);
    nodes[4].setPosition(0.7*pos);
    nodes[4].setVelocity(vel);
    pos<< 1, -1, 1;
    nodes[5].setInitialPosition(pos);
    nodes[5].setPosition(0.7*pos);
    nodes[5].setVelocity(vel);
    pos<< 1, 1, 1;
    nodes[6].setInitialPosition(pos);
    nodes[6].setPosition(0.7*pos);
    nodes[6].setVelocity(vel);
    pos<< -1, 1, 1;
    nodes[7].setInitialPosition(pos);
    nodes[7].setPosition(0.7*pos);
    nodes[7].setVelocity(vel);

    Hexahedron hex;
    hex.nodes= nodes;
    hex.NodesIndex=ind;

    hex.preComputeB();
    hex.setDeformationGradient();
    hex.setVolume();
    
    systemFEM sysHex(&hex,nodes,1,8);
    sysHex.initialize();
    IntegratorFEM Integ;
    Integ.pS= &sysHex;

    MeshTri3D mesh;
    MatrixXd A,V1;
    A= sysHex.getPositions();
    A.resize(3,8);
    V1=A.transpose();
	mesh.setName("test mesh");
	mesh.updatePointBuffer(V1);

    while(!window.shouldClose())
    {

		Integ.doStepImplicit();
    	A= sysHex.getPositions();
    	A.resize(3,8);
    	V1=A.transpose();

        mesh.updatePointBuffer(V1);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    };


}

}
