
#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>
#include <utils/io/readOBJ.hpp>
#include <utils/RemoveRows.hpp>
#include <mains/ParticleSystem/Integrator.hpp>
#include <mains/FEM/Integrator_fem.hpp>
#include <mains/FEM/Integrator_fem.hpp>
#include <mains/FEM/tetrahedron_fem.hpp>
#include <mains/FEM/force_fem.hpp>
#include <mains/FEM/force_type/NeoHookean.hpp>
#include <mains/FEM/system_fem.hpp>
#include <utils/ReturnTetEdges.hpp>
#include <utils/AssignNodeMass.hpp>
#include <igl/copyleft/tetgen/tetrahedralize.h>

using namespace std;


namespace Mains
{


void test1()
{  
    Window window("Visualizer");
    window.setSize(1200, 700);
    
    DenseMatrixX V; // Vertices
    DenseMatrixXi E, F; // Edges, Faces

    // mesh 1: simple triangle
    /**/
    V.resize(3,3);
    V.row(0) = Vec3(1.0, 0.0, 0.0);
    V.row(1) = Vec3(0.0, 1.0, 0.0);
    V.row(2) = Vec3(0.0, 0.0, 1.0);

    E.resize(3,2);
    E.row(0) = Vec2i(0,1);
    E.row(1) = Vec2i(0,2);
    E.row(2) = Vec2i(1,2);

    F.resize(1,3);
    F.row(0) = Vec3i(0,1,2);
    /**/

    // mesh 2: sphere mesh
    /*
    utils::readOBJ("meshes/sphere.obj", V, E, F);
    /**/
    
    MeshTri3D mesh;
    mesh.setName("test mesh");
    mesh.updatePointBuffer(V);
    mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
    mesh.addFaceLayer("Faces")->updateFaceBuffer(F);
    mesh.initRenderable();

    // extra 1: add extra edge layers
    /*
    DenseMatrixXi E2;
    E2.resize(2,2);
    E2.row(0) = Vec2i(0,1);
    E2.row(1) = Vec2i(1,2);
    mesh.addEdgeLayer("Extra Edges")->updateEdgeBuffer(E2);
    /**/

    // extra 2: add color masks for a subset of vertices
    /*
    PointMask *mask = mesh.addPointMask("Mask");
    mask->m_firstPointIndex = 0;
    mask->m_numPoints = 2;
    /**/
    
    while(!window.shouldClose())
    {
        V.row(0) += Vec3(0.00001, 0.0, 0.0);
        mesh.updatePointBuffer(V);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    }
}

void test2()
{  
    Window window("Visualizer");
    window.setSize(1200, 700);
    
    DenseMatrixX V1, V2; // Vertices
    DenseMatrixXi E1, E2, F1, F2; // Edges, Faces

    utils::readOBJ("meshes/sphere.obj", V1, E1, F1);
    utils::readOBJ("meshes/monkey.obj", V2, E2, F2);
    
    MeshTri3D sphere;
    sphere.setName("sphere mesh");
    sphere.updatePointBuffer(V1);
    sphere.addEdgeLayer("Edges")->updateEdgeBuffer(E1);
    sphere.addFaceLayer("Faces")->updateFaceBuffer(F1);

    MeshTri3D monkey;
    monkey.setName("monkey mesh");
    monkey.updatePointBuffer(V2);
    monkey.addEdgeLayer("Edges")->updateEdgeBuffer(E2);
    monkey.addFaceLayer("Faces")->updateFaceBuffer(F2);

    while(!window.shouldClose())
    {
        window.clear();
        window.drawGrid();
        window.draw(&sphere);
        window.draw(&monkey);

        window.drawGui(&sphere);
        window.drawGui(&monkey);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    }
}

void mytest2()
{
	  Window window("Visualizer");
	    window.setSize(1200, 700);

	    DenseMatrixX V1, V2; // Vertices
	    DenseMatrixXi E1(2,2); // Edges, Faces
	    DenseMatrixXi F1(1,3); // Edges, Faces
	    Integrator ite;
	    Particle particles[3];
	    Force interactions[2];
	    E1<<0,1,1,2;
	    F1<<0,1,2;


particleSystem pS(3,2, particles, interactions);

ite.pS=&pS;

Vector3d pos,vel;
pos<< 0, 0, 1;
vel<< 0, 0, 0;

pS.Particles[0].setPosition(pos);
pos<< 0, 1, 0;
pS.Particles[1].setPosition(pos);
pos<< 1, 0, 0;
pS.Particles[2].setPosition(pos);


pS.Particles[0].setVelocity(vel);
pS.Particles[1].setVelocity(vel);
pS.Particles[2].setVelocity(vel);

pS.Particles[0].setMass(1);
pS.Particles[1].setMass(1);
pS.Particles[2].setMass(1);
pS.setPositions();
pS.setVelocities();
pS.Interactions[0].setParticles(pS.Particles, 0, 1);
pS.Interactions[1].setParticles(pS.Particles, 1, 2);


pS.setMass();
	pS.setPositions();
	pS.setVelocities();
	pS.setForceSpatialDerivatives();

	DenseMatrixX V; // Vertices
	DenseMatrixXi E=E1; // Edges, Faces
	V=pS.getPositions();
	V.resize(3,3);
	DenseMatrixX a=V.transpose();


	MeshTri3D sphere;
	sphere.setName("sphere test mesh");
	sphere.updatePointBuffer(V);
	sphere.addEdgeLayer("Edges")->updateEdgeBuffer(E1);
	sphere.addFaceLayer("Faces")->updateFaceBuffer(F1);

	while(!window.shouldClose())
	    {


		   ite.doStepImplicit();
	       V=pS.getPositions();
		   V.resize(3,3);
		   a=V.transpose();

		   sphere.updatePointBuffer(a);

	        window.clear();
	        window.drawGrid();
	        window.draw(&sphere);

	        window.drawGui(&sphere);
	        window.renderGui();
	        window.swapBuffers();

	        window.pollEvents();
	        window.manageEvents();
	    }

};

void mytest3()
{
Integrator ite;
Particle particles[3];
Force interactions[2];
particleSystem pS(3, 2, particles, interactions);
ite.pS= &pS;
Vector3d pos,vel;
pos<< 0, 0, 1;
vel<< 0, 0, 0;
Particle p;

pS.Particles[0].setPosition(pos);
pos<< 0, 1, 0;
pS.Particles[1].setPosition(pos);
pos<< 1, 0, 0;
pS.Particles[2].setPosition(pos);


pS.Particles[0].setVelocity(vel);
pS.Particles[1].setVelocity(vel);
pS.Particles[2].setVelocity(vel);

pS.Particles[0].setMass(1);
pS.Particles[1].setMass(1);
pS.Particles[2].setMass(1);
pS.setPositions();
pS.setVelocities();
pS.Interactions[0].setParticles(pS.Particles, 0, 1);
pS.Interactions[1].setParticles(pS.Particles, 1, 2);


pS.setForceSpatialDerivatives();


};

void mytest4()
{

	Window window("Visualizer");
	window.setSize(1200, 700);

	DenseMatrixX V1; // Vertices
	DenseMatrixXi E1, F1; // Edges, Faces

	utils::readOBJ("meshes/sphere.obj", V1, E1, F1);

	int np=V1.rows();
	int ni=E1.rows();

	Particle particles[np];
	Force interactions[ni];
	for(int i=0; i<np; i++)
	{ Vector3d pos;
	  pos= 20*V1.block<1,3>(i, 0) ;
	  Vector3d vel;
	  vel<<0 , 0 , 0;
	  particles[i].setPosition(pos);
	  particles[i].setVelocity(vel);
	  particles[i].setMass(1);
	};
	for(int i=0; i<ni; i++)
	{
		interactions[i].setParticles(particles,E1(i,0),E1(i,1));
	};

	particleSystem pS(np, ni, particles, interactions);
	Integrator ite;
	ite.pS= &pS;

	pS.setMass();
	pS.setPositions();
	pS.setVelocities();
	pS.setForceSpatialDerivatives();

	DenseMatrixX V; // Vertices
	DenseMatrixXi E=E1; // Edges, Faces
	V=pS.getPositions();
	V.resize(3,np);
	DenseMatrixX a=V.transpose();


	MeshTri3D sphere;
	sphere.setName("sphere test mesh");
	sphere.updatePointBuffer(V);
	sphere.addEdgeLayer("Edges")->updateEdgeBuffer(E1);
	sphere.addFaceLayer("Faces")->updateFaceBuffer(F1);

	while(!window.shouldClose())
	    {


		   ite.doStepImplicit();
	       V=pS.getPositions();
		   V.resize(3,np);
		   a=V.transpose();

		   sphere.updatePointBuffer(a);

	        window.clear();
	        window.drawGrid();
	        window.draw(&sphere);

	        window.drawGui(&sphere);
	        window.renderGui();
	        window.swapBuffers();

	        window.pollEvents();
	        window.manageEvents();
	    }
}

void mytest5()
{
	Window window("Visualizer");
	window.setSize(1200, 700);

	DenseMatrixX V1,V; // Vertices
	DenseMatrixXi E1, F1,Esort; // Edges, Faces

	utils::readOBJ("meshes/sphere.obj", V1, E1, F1);

	DenseMatrixX Vtet,I; // Vertices
	DenseMatrixXi Ttet, Etet, Ftet; // Edges, Faces

	int status = igl::copyleft::tetgen::tetrahedralize(V1, F1, "pq1.01Y", Vtet, Ttet, Ftet);
	VectorXd particle_mass;

	particle_mass= NodeMass(Vtet,Ttet);


	Etet=TetEdges(Ttet);
	int np=Vtet.rows();
	int ni=Etet.rows();

	Particle particles[np];
	Force interactions[ni];
	for(int i=0; i<np; i++)
		{ Vector3d pos;
		  pos= V1.block<1,3>(i, 0) ;
		  Vector3d vel;
		  vel<<0 , 0 , 0;
		  particles[i].setPosition(pos);
		  particles[i].setVelocity(vel);
		  particles[i].setMass(particle_mass[i]);
		};
		for(int i=0; i<ni; i++)
		{
			interactions[i].setParticles(particles,Etet(i,0),Etet(i,1));
		};

		particleSystem pS(np, ni, particles, interactions);
		Integrator ite;
		ite.pS= &pS;

		pS.setMass();
		pS.setPositions();
		pS.setVelocities();
		pS.setForceSpatialDerivatives();

	V=pS.getPositions();
	V.resize(3,np);
	DenseMatrixX a=V.transpose();

		MeshTri3D sphere;
		sphere.setName("sphere test mesh");
		sphere.updatePointBuffer(V);
		sphere.addEdgeLayer("Edges")->updateEdgeBuffer(Etet);
		sphere.addFaceLayer("Faces")->updateFaceBuffer(Ftet);

		while(!window.shouldClose())
		    {


			   ite.doStepImplicit();
		       V=pS.getPositions();
			   V.resize(3,np);
			   a=V.transpose();

			   sphere.updatePointBuffer(a);

		        window.clear();
		        window.drawGrid();
		        window.draw(&sphere);

		        window.drawGui(&sphere);
		        window.renderGui();
		        window.swapBuffers();

		        window.pollEvents();
		        window.manageEvents();
		    }

}

void mytest6()
{
	Node nodes[5];
	Vector3d pos;
	Vector3d vel;
	pos<<0,0,0;
	vel<<0,0,0;
	nodes[0].setInitialPosition(pos);
	pos<<0,0,0.5;
	nodes[0].setPosition(pos);
	nodes[0].setVelocity(vel);

	pos<<1,0,0;
	nodes[1].setInitialPosition(pos);
	pos<<1,0,0;
	nodes[1].setPosition(pos);
	nodes[1].setVelocity(vel);

	pos<<0,1,0;
	nodes[2].setInitialPosition(pos);
	pos<<0,1,0;
	nodes[2].setPosition(pos);
	nodes[2].setVelocity(vel);

	pos<<0,0,1;
	nodes[3].setInitialPosition(pos);
	pos<<0,0,1;
	nodes[3].setPosition(pos);
	nodes[3].setVelocity(vel);

	pos<<1,1,1;
	nodes[4].setInitialPosition(pos);
	pos<<1,1,1.21;
	nodes[4].setPosition(pos);
	nodes[4].setVelocity(vel);

	Tetrahedron tet[2];

	tet[0].NodesIndex<<0,1,2,3;
	tet[1].NodesIndex<<4,2,3,1;

	tet[0].nodes=nodes;
	tet[1].nodes=nodes;

	tet[0].setVolume();
	tet[1].setVolume();//always set volume first

	tet[0].preComputeB();
	tet[0].setDeformationGradient();
	tet[1].preComputeB();
	tet[1].setDeformationGradient();

	LinearForce testForce(&tet[1]);
	testForce.miu=1;
	testForce.lambda=1;
	testForce.setAll();
	systemFEM sss(tet,nodes,2,5);




	sss.setMass();
	sss.setPositions();
	sss.setVelocities();
	sss.setForces();
	IntegratorFEM Integ;
	Integ.pS= &sss;


	Window window("Visualizer");
	window.setSize(1200, 700);

	DenseMatrixX V,A; // Vertices
	DenseMatrixXi E, F; // Edges, Faces

	A= sss.getPositions();
	A.resize(3,5);
	V=A.transpose();
	E.resize(9,2);
	E.row(0) = Vec2i(0,1);
	E.row(1) = Vec2i(0,2);
	E.row(2) = Vec2i(0,3);
	E.row(3) = Vec2i(4,1);
	E.row(4) = Vec2i(4,2);
	E.row(5) = Vec2i(4,3);
	E.row(6) = Vec2i(2,1);
	E.row(7) = Vec2i(3,2);
	E.row(8) = Vec2i(1,3);

    F.resize(6,3);
    F.row(0) = Vec3i(0,1,2);
    F.row(1) = Vec3i(0,1,3);
    F.row(2) = Vec3i(0,2,3);
    F.row(3) = Vec3i(2,4,1);
    F.row(4) = Vec3i(3,4,2);
    F.row(5) = Vec3i(1,4,3);

    MeshTri3D mesh;
    mesh.setName("test mesh");
    mesh.updatePointBuffer(V);
    mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
    mesh.addFaceLayer("Faces")->updateFaceBuffer(F);

    mesh.initRenderable();


    int fix[3]={1,2,3};
    Integ.fixed_points=fix;
    Integ.n_fixed_points=3;


	while(!window.shouldClose())
    {
    	Integ.doStepImplicit();
    	A= sss.getPositions();
    	A.resize(3,5);
    	V=A.transpose();


        mesh.updatePointBuffer(V);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    }




}
void mytest7()
{
	Window window("Visualizer");
	window.setSize(1200, 700);

	DenseMatrixX V1,V,A; // Vertices
	DenseMatrixXi E1, F1,Esort; // Edges, Faces

	utils::readOBJ("meshes/cubefine.obj", V1, E1, F1);


	DenseMatrixX Vtet,I; // Vertices
	DenseMatrixXi Ttet, Etet, Ftet; // Edges, Faces

	int status = igl::copyleft::tetgen::tetrahedralize(V1, F1, "pq1e-2Y", Vtet, Ttet, Ftet);
	Etet=TetEdgesFEM(Ttet);
	int np=Vtet.rows();
	int nt=Ttet.rows();

	Node nodes[np];
	Tetrahedron  tet[nt];
	systemFEM sss(tet,nodes,nt,np);
	IntegratorFEM Integ;
	Integ.pS= &sss;

	cout<<np<<"nodes"<<endl;
	cout<<nt<<"tets"<<endl;

	for(int i=0; i<np; i++)
			{ Vector3d pos,pos1;
			  pos= Vtet.block<1,3>(i, 0) ;
			  Vector3d vel;
			  vel<<0 , 0 , 0;

			  nodes[i].setInitialPosition(pos);
			  pos1=0.7*pos;
			  nodes[i].setPosition(pos1);
			  nodes[i].setVelocity(vel);
			};

	cout<<Ttet<<endl;

	for(int i=0; i<nt; i++)
			{
				tet[i].NodesIndex=Ttet.row(i);
				cout<<tet[i].NodesIndex<<'h'<<endl;
				tet[i].nodes=nodes;
				tet[i].setVolume();
				tet[i].preComputeB();
				tet[i].setDeformationGradient();
			};

	sss.setMassUnlumped();
	cout<<sss.getMass()<<endl;
	sss.setPositions();
	sss.setVelocities();
	sss.setForces();

	MeshTri3D mesh;
	mesh.setName("test mesh");
	mesh.updatePointBuffer(V1);
	mesh.addEdgeLayer("Edges")->updateEdgeBuffer(Etet);
	mesh.addFaceLayer("Faces")->updateFaceBuffer(F1);

	VectorXd a;
	a= sss.getPositions();
	a=sss.getForces();
	MatrixXd MM;
	mesh.initRenderable();

	while(!window.shouldClose())
    {

		Integ.doStepImplicit();
    	A= sss.getPositions();
    	A.resize(3,np);
    	V1=A.transpose();
    	sss.setPotentialEnergy();
    	cout<<sss.getPotentialEnergy()<<"potential energy"<<endl;

        mesh.updatePointBuffer(V1);

        window.clear();
        window.drawGrid();
        window.draw(&mesh);

        window.drawGui(&mesh);
        window.renderGui();
        window.swapBuffers();

        window.pollEvents();
        window.manageEvents();
    }

}
}
